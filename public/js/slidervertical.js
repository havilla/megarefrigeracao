busSlider()

//Slider Vertical
// $(document).ready(function() {
//   $('.bxslider').bxSlider({
//     mode: 'vertical',
//     slideMargin: 5
//   });
// });

function busSlider(){
	$.ajax({
		type: 'GET',
		contentType: "application/json",
		url: "/slidersecund/buscar",
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_painel_bcStore')).token},
		beforeSend: function(){
			// loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			// console.log(data)
		$.each(data[0].imagens, function(key){
			if (((key) % 3) === 0) {
				$("#primary .bxslider").append(
					'<li style="float: none; list-style: none; position: relative; width: 850px; margin-bottom: 5px;" class="bx-clone '+key+'">'+
						'<div class="containerImg">'+
							'<img src="'+data[0].imagens[key].anexo+'"/>'+
							'<figcaption class="figImg">'+
								'<p class="nomeProdSlid">'+data[0].imagens[key].nome+'</p>'+
							'</figcaption>'+
						'</div>'+
					'</li>'
					)			
			}else{
				$("#primary .bxslider li:last-child").append(
				'<div class="containerImg">'+
					'<img src="'+data[0].imagens[key].anexo+'"/>'+
					'<figcaption class="figImg">'+
						'<p class="nomeProdSlid">'+data[0].imagens[key].nome+'</p>'+
					'</figcaption>'+
				'</div>'
				)
			}	
		});
		$(document).ready(function() {
		  $('.bxslider').bxSlider({
		    mode: 'vertical',
		    slideMargin: 5,
		    auto: true,
		    pause: 15000
		    // speed: 5000
		  });
		});
		},
		error: function(jqXHR, textStatus){
			// alertGlobal("Tente novamente!","" , "/images/global/alert.png","FECHAR");
		}
	});
}