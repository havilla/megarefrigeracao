// document.querySelector('#bordaForaCor').addEventListener("change", CorBordaFora);

// function CorBordaFora (argument) {
// 	// console.log(document.getElementById("cCor").value)
// 	var cor = document.getElementById("bordaForaCor").value;
// 	document.querySelector("#primary").style.background = cor;
// }
var bt_cor1 = document.querySelector("#bordaForaCor");
var bt_transparencia1 = document.querySelector("#input-traparencia-carr");

var alvo1 = document.querySelector("#primary");
	// Adcionando evente input nos botoes
	bt_cor1.addEventListener("input",adcionaCorBordaFora);
	bt_transparencia1.addEventListener("input",adcionaCorBordaFora);

function adcionaCorBordaFora(){
	alvo1.style.background = bt_cor1.value;
	var color = alvo1.style.background;
	var transparent = bt_transparencia1.value;
	var corFinal = "rgba"+(color.slice(3, color.length-1))+(","+transparent+")")
	alvo1.style.background = corFinal;
}
//------------------Muda cor da borda de dentro do carrosel---------//
document.querySelector('#bordaDentroCor').addEventListener("change", CorBordaDentro);

function CorBordaDentro (argument) {
	// console.log(document.getElementById("cCor").value)
	var cor = document.getElementById("bordaDentroCor").value;
	document.querySelector(".bx-wrapper .bx-wrapper").style.background = cor;
}