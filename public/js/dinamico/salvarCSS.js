function JsonCSS() {
	var css = {};
	css.barra = {};
	css.barra.background = document.querySelector('#barra').style.backgroundColor;
	css.barra.color = document.querySelector('#barraCor').value;
	css.circle = {};
	css.circle.border = document.querySelector('#contornoCor').value;
	css.informacao = {};
	css.informacao.borderTop = document.querySelector('#contornoCor').value;
	css.barraFonte = {};
	css.barraFonte.color = document.querySelector('#fonteCor').value;
	css.redeSocial = {};
	css.redeSocial.backgroundColor = document.querySelector('#redeSocialCor').value;
	css.redeSocialHover = {};
	css.redeSocialHover.backgroundColor = document.querySelector('#hoverCor').value;
//--------------------------Pega valores do Menu-----------------------//
	css.menu = {};
	css.menu.background = document.querySelector('#menuCont').style.backgroundColor;
	css.menuFonte = {}
	css.menuFonte.color =  document.querySelector('#fonteMenuCor').value;
	css.menuHover = {};
	css.menuHover.color = document.querySelector('#hoverCorFonte').value;
//--------------------------Pega valores do carrosel------------------//
	css.carrosel = {};
	css.carrosel.background = document.querySelector('#primary').style.background;
	css.carroselBorda = {}
	css.carroselBorda.backgroundBorder = document.querySelector('#bordaDentroCor').value;
	
	// css.nomeImg = document.querySelector("#logo").value.split("\\").pop();
	css.src = document.querySelector('#logoImg img').src;
	// console.log(css)
	barraInfo(JSON.stringify(css));
}
function barraInfo(param){
	// alert("salvar as fotos");
	$.ajax({
	 	type: 'POST',
		contentType: "application/json",
		url: "/salvarBarra",
		data: param,
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_cliente_bcStore')).token},
		beforeSend: function(){
			// loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			alertGlobal("Salvo com Sucesso!","","","FECHAR");
			// $("#loading").remove();
		
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			// $("#loading").remove();
		}
	});
}
// $(function () {
    // var form;
    // $('#logo').change(function (event) {
    //     form = new FormData();
    //     form.append('photos', event.target.files[0]); // para apenas 1 arquivo
    //     //var name = event.target.files[0].content.name; // para capturar o nome do arquivo com sua extenção
    // });

    // $('#btnSalvar').click(function () {
    // 	console.log(form)
    //     $.ajax({
    //         url: '/salvarLogoMarca', // Url do lado server que vai receber o arquivo
    //         data: form,
    //         processData: false,
    //         contentType: false,
    //         type: 'POST',
    //         success: function (data) {
    //             // utilizar o retorno
    //         }
    //     });
    // });
// });