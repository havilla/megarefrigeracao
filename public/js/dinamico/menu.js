//--------------Muda cor do menu-----------------//
// document.querySelector('#menuCor').addEventListener("change", corMenu);
// function corMenu (argument) {
// 	// console.log(document.getElementById("cCor").value)
// 	var cor = document.getElementById("menuCor").value;
// 	document.querySelector("#menuCont").style.background = cor;
// }
window.onload = function(){
	adcionaCorBarraMenu();
	adcionaCor();
	adcionaCorBordaFora();
}
var bt_cor2 = document.querySelector("#menuCor");
var bt_transparencia2 = document.querySelector("#input-traparencia-menu");

var alvo2 = document.querySelector("#menuCont");
	// Adcionando evente input nos botoes
	bt_cor2.addEventListener("input",adcionaCorBarraMenu);
	bt_transparencia2.addEventListener("input",adcionaCorBarraMenu);

function adcionaCorBarraMenu(){
	alvo2.style.backgroundColor = bt_cor2.value;
	var color = alvo2.style.backgroundColor;
	var transparent = bt_transparencia2.value;
	var corFinal = "rgba"+(color.slice(3, color.length-1))+(","+transparent+")")
	alvo2.style.backgroundColor = corFinal;
}
//--------------Muda cor da fonte dos links do menu-----------------//
document.querySelector('#fonteMenuCor').addEventListener("change", corFonteMenu);
function corFonteMenu (argument) {
	// console.log(document.getElementById("cCor").value)
	var cor = document.getElementById("fonteMenuCor").value;
	var fonte = document.querySelectorAll(".ListaMenu h4");
	fonte.forEach( function(element, index) {
		fonte[index].style.color = cor;
	});
}
//--------------Muda cor do hover das letras do menu-----------------//
var campo = document.querySelectorAll("#navbar>ul>li")
campo.forEach( function(element, index) {
	campo[index].addEventListener("mousemove", corHover);
	campo[index].addEventListener("mouseout", removeHover);
});
function corHover(argument){
	var cor = document.getElementById("hoverCorFonte").value;
	this.querySelector("h4").style.color = cor;
	this.querySelector("h4").style.borderBottom = "11px solid"+cor;
}
function removeHover(argument){
	this.querySelector("h4").style.color = document.getElementById("fonteMenuCor").value;
	this.querySelector("h4").style.borderBottom = "";
}