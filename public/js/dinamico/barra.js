//--------------Muda cor da barra de informação-----------------//
// document.querySelector('#barraCor').addEventListener("change", pegarCorBarra);
// function pegarCorBarra (argument) {
// 	// console.log(document.getElementById("cCor").value)
// 	var cor = document.getElementById("barraCor").value;
// 	document.querySelector("#barra").style.background = cor;
// 	var informacao = document.querySelectorAll(".informacao");
// 	informacao.forEach( function(element, index) {
// 		informacao[index].style.backgroundColor = cor;
// 	});
// }
// window.onload = function(){
// 	adcionaCor();
// }
var bt_cor = document.querySelector("#barraCor");
var bt_transparencia = document.querySelector("#input-traparencia");

var alvo = document.querySelector("#barra");
	// Adcionando evente input nos botoes
	bt_cor.addEventListener("input",adcionaCor);
	bt_transparencia.addEventListener("input",adcionaCor);

function adcionaCor(){
	alvo.style.backgroundColor = bt_cor.value;
	var color = alvo.style.backgroundColor;
	var transparent = bt_transparencia.value;
	var corFinal = "rgba"+(color.slice(3, color.length-1))+(","+transparent+")")
	alvo.style.backgroundColor = corFinal;
	var informacao = document.querySelectorAll(".informacao");
	informacao.forEach( function(element, index) {
		informacao[index].style.backgroundColor = color;
	});
}

//--------------Muda cor da fonte de informação-----------------//
document.querySelector('#fonteCor').addEventListener("change", pegarCorFonte);
function pegarCorFonte (argument) {
	// console.log(document.getElementById("cCor").value)
	var cor = document.getElementById("fonteCor").value;
	var fonte = document.querySelectorAll(".informacao h4");
	var svg = document.querySelectorAll(".circle svg");
	fonte.forEach( function(element, index) {
		fonte[index].style.color = cor;
		svg[index].style.color = cor;
	});
}
//--------------Muda cor do contorno das informação da barra-----------------//
document.querySelector('#contornoCor').addEventListener("change", pegarCorContorno);
function pegarCorContorno (argument) {
	// console.log(document.getElementById("cCor").value)
	var cor = document.getElementById("contornoCor").value;
	var contorno = document.querySelectorAll(".informacao");
	var contornoCircle = document.querySelectorAll(".circle");
	contorno.forEach( function(element, index) {
		contorno[index].style.borderBottom = '3px solid'+cor;
		contorno[index].style.borderTop = '3px solid'+cor;
		contorno[index].style.borderRight = '3px solid'+cor;
		contornoCircle[index].style.border = '3px solid'+cor;
	});
}
//--------------Muda cor dos icones das redes sociais-----------------//
document.querySelector('#redeSocialCor').addEventListener("change", redeSocialCor);
function redeSocialCor (argument) {
	// console.log(document.getElementById("cCor").value)
	var cor = document.getElementById("redeSocialCor").value;
	var icone = document.querySelectorAll(".Face, .Google, .Linkdin");
	icone.forEach( function(element, index) {
		icone[index].style.backgroundColor = cor;
	});
}
//--------------Muda cor dos hovers da barra-----------------//
var campo = document.querySelectorAll(".Social2>div")
campo.forEach( function(element, index) {
	campo[index].addEventListener("mousemove", corHover);
	campo[index].addEventListener("mouseout", removeHover);
});
var campo2 = document.querySelectorAll(".orcamentoHover")
campo2.forEach( function(element, index) {
	campo2[index].addEventListener("mousemove", corHover2);
	campo2[index].addEventListener("mouseout", removeHover2);
});
function corHover(argument){
	var cor = document.getElementById("hoverCor").value;
	this.style.backgroundColor = cor;
}
function removeHover(argument){
	this.style.backgroundColor = document.getElementById("redeSocialCor").value;
}
function corHover2(argument){
	var cor = document.getElementById("hoverCor").value;
	this.querySelector(".circle").style.backgroundColor = cor;
	this.querySelector(".informacao").style.backgroundColor = cor;
}
function removeHover2(argument){
	this.querySelector(".circle").style.backgroundColor = "";
	this.querySelector(".informacao").style.backgroundColor = document.getElementById("barraCor").value;
}
