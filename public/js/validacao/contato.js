function ValidarDadosContato() {
	if ($('.nome').val() == "") {
		alertGlobal("Campo Nome Vazio!");
	} else if ($('#form-contato .email').val() == "") {
		alertGlobal("Campo E-mail Vazio!");
	}else if (!validacaoEmail(document.querySelector('#form-contato .email'))) {
		alertGlobal("Campo E-mail Invalido!");
	}else if ($('#form-contato .telefone').val() == 0) {
		alertGlobal("Campo Telefone Vazio!");
	}else if ($('#form-contato .assunto').val() == "") {
		alertGlobal("Campo Assunto Vazio!");
	}else if ($('#form-contato .mensagem').val() == "") {
		alertGlobal("Campo Mensagem Vazio!");
	}else if(!$('input[type="radio"]:checked').length){
		alertGlobal("Marque uma das opçoes");
	}else{
		return true
	}
}