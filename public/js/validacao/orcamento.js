$(function() {
	$("#file-original").on('change', function(){
		if($(".baixoUpload .galeriaDesh").length==2){
			$("#file-original").attr('disabled', 'disabled');
			$(".foto > div").addClass('hiddenn');
			$(".foto").addClass('foto2');
			$("#file-original").css({display: 'none'});
		}
	});
});
$("#orcamento").on('click', '.botaoEnviarOrcmento', function(event) {
	// alert("chego")
	ValidarProdutos();
});
$("#orcamento").on('click', '#enviarOrc', function(event) {
	if(ValidarDados()){
		if($('input[name="opcoesOrc"]:checked').val()=="produto"){
			ValidarTabela()
		}else{
			ValidarProdutosAssisT()
		}
	}
	// ValidarDados();
	// ValidarProdutosAssisT();

});
function addLista(Nome, Descricao, Quantidade, Foto) {
	$("tbody").append(
		'<tr>' +
		'<td class="nomePro"><div style="margin: 15px 0px;">' + Nome + '</div></td>' +
		'<td class="descricaoPro"><div style="margin: 15px 0px;">' + Descricao + '</div></td>' +
		'<td class="qtdePro"><div style="margin: 15px 0px;">' + Quantidade + '</div></td>' +
		'<td class="fotoOrc" style="width: 211px;">' + Foto +
		// '<div>'+
		// 	'<img src="'+Foto+'">'+
		// '</div>'+
		'</td>' +
		'<td>' +
		'<div>' +
		'<button type="button" class="btn btn-primary btnExcluir"><svg class="icon icon-lixeira"><use xlink:href="#icon-lixeira"></use></svg></button>' +
		'</div>' +
		'</td>' +
		'</tr>'
	);
	$("#orcamento").on("click", ".btnExcluir", function(e) {
		$(this).closest('tr').remove();
		if(!$("#orcamento tbody tr").length){
			$('#enviarOrc').attr('disabled', 'disabled');
		}
	});
}

//Validação

function ValidarDados() {
	if ($('#nome').val() == "") {
		alertGlobal("Campo Nome Vazio!");
	} else if ($('#email').val() == "") {
		alertGlobal("Campo E-mail Vazio!");
	}else if (!validacaoEmail(document.querySelector('.form-orcamento #email'))) {
		alertGlobal("Campo E-mail Invalido!");
	}else if ($('#telefone').val() == 0) {
		alertGlobal("Campo Telefone Vazio!");
	}else if(!$('input[name="opcoesOrc"]:checked').length){
		alertGlobal("Marque uma das opçoes");
	}else{
		return true
		// ValidarTabela();
	}
}
function ValidarProdutos() {
	// alert("validar produtos")
	if ($('#proNome').val() == "") {
		alertGlobal("Campo Nome Vazio!");
		// alert('Campo Nome Vazio!')
	} else if ($('#proDescricao').val() == "") {
		alertGlobal("Campo Descriçao Vazio!");
		// alert('Campo Descriçãos Vazio!')
	} else if ($('#proQtdade').val() == 0) {
		alertGlobal("Campo Quantidade Vazio!");
		// alert('Campo Quantidade 0')
	} else {
		addLista($("#proNome").val(), $("#proDescricao").val(), $("#proQtdade").val(), $(".baixoUpload").html());
		$('#enviarOrc').removeAttr('disabled');
		$("#produto input[type=text], input[type=number], input[type=file], textarea").val("");
		$(".baixoUpload").html("");
		AtivarBotao();
	}
}


// Validar parte de produtos da assistencia tecnica
function ValidarProdutosAssisT() {
	if ($('#proNome').val() == "") {
		alertGlobal("Campo Nome Vazio!");
		// alert('Campo Nome Vazio!')
	} else if ($('#proDescricao').val() == "") {
		alertGlobal("Campo Descriçao Vazio!");
		// alert('Campo Descriçãos Vazio!')
	}else {
		buscarDados();
	}
}

function ValidarTabela(){
	if($("#orcamento tbody tr").length || $('input[name="opcoesOrc"]:checked').val()=="assistTecnica"){
		// alert('Salvo com sucesso')
		buscarDados();
	}else{
		alertGlobal("Não possui itens na tabela");
		// alert('Não possui itens na tabela')
	}
}
function buscarDados(){
	var orcamento={};
	orcamento.categoria=$('input[name="opcoesOrc"]:checked').val();
	orcamento.nome=$("#nome").val();
	orcamento.email=$("#email").val();
	orcamento.telefone=$("#telefone").val();
	orcamento.produtos=[];
	if($('input[name="opcoesOrc"]:checked').val()=="produto"){
		$('tbody tr').each(function(i){
			var produtos = {};
				produtos.nomePro = $(this).find('.nomePro div').text();
				produtos.descricaoPro    = $(this).find('.descricaoPro div').text();
				produtos.qtdePro    = $(this).find('.qtdePro div').text();
				produtos.fotos = [];
			$(this).find('.fotoOrc .galeriaDesh').each(function(i){
				var fotos = {};
				fotos.fotosPro = $(this).find('img').attr('src');
				produtos.fotos.push(fotos);
			});
			orcamento.produtos.push(produtos);

		});
	salvarOrc(JSON.stringify(orcamento));	
	}else{
		var produtos = {};
			produtos.nomePro = $("#proNome").val();
			produtos.descricaoPro    = $("#proDescricao").val();
			produtos.fotos = [];
			$(".baixoUpload").find('.galeriaDesh').each(function(i){
				var fotos = {};
				fotos.fotosPro = $(this).find('input').val();
				fotos.fotosNome = $(this).find('.uploadArquivo').text();
				produtos.fotos.push(fotos);
			});
		orcamento.produtos.push(produtos);
		salvarOrc(JSON.stringify(orcamento));	
	}
}
function salvarOrc(orcamento){
	$.ajax({
	 	type: 'POST',
		contentType: "application/json",
		url: "/orcamento",
		data: orcamento,
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_cliente_bcStore')).token},
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			alertGlobal(data.message,"","","FECHAR");
			if(data.success){
				$("#produto input[type=text], textarea").val("");
				$(".galeriaDesh").remove();
				$("tbody").html("");	
			}
			$("#loading").remove();
			// limparCampos();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
		}
	});

}
function limparCampos(){
	AtivarBotao();
	$("#produto input[type=text],input[type=number], textarea").val("");
	$(".galeriaDesh").remove();
	$("tbody").html("");

	// $(".baixoUpload").html("");
}
function validacaoEmail(field) {
	usuario = field.value.substring(0, field.value.indexOf("@"));
	dominio = field.value.substring(field.value.indexOf("@") + 1, field.value.length);

	if ((usuario.length >= 1) &&
		(dominio.length >= 3) &&
		(usuario.search("@") == -1) &&
		(dominio.search("@") == -1) &&
		(usuario.search(" ") == -1) &&
		(dominio.search(" ") == -1) &&
		(dominio.search(".") != -1) &&
		(dominio.indexOf(".") >= 1) &&
		(dominio.lastIndexOf(".") < dominio.length - 1)) {
		// document.getElementById("msgemail").innerHTML = "E-mail válido";
		return true;
	} else {
		// document.getElementById("msgemail").innerHTML = "<font color='red'>E-mail inválido </font>";
		// alertGlobal("Email Invalido!","FECHAR");
		return false;
	}
}


