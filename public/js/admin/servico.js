$(function(){	
	busImgsServ();
	$('.btnSalImgs').click(function(event) {
		validarImgs();
	});	
	$('.rodaPe .btnLimpar').click(function(event) {
		limparCampos();
	});
	
/////////////////////////////////////////////////
	// $('.btnCarregarFoto').unbind("click");
	$(".btnCarregarFoto").bind( "click", function() {
		if(!$(this).hasClass('upl_Ativo')){
			$(this).addClass('upl_Ativo');
			openUpload();
		}
	});
	$('.offUpload').click(function(){
		offUpload();	});
	$(document).off('change', '#file-original')
	$(document).on('change', '#file-original', function(){	
		readImage( this );
	})
	$("#btn_Confirmar").off();
	$("#btn_Confirmar").on('click',function(event) {
		// var validar = false;
		// $(".cadSlide").each(function(i){
		// 	if($(this).find('input').val()==""){
		// 		$(this).find('input').addClass('zeroProduto')
		// 		alertGlobal("Referencia obrigatoria", "vazio", "","FECHAR");
		// 		validar = false;
		// 	}else{
		// 		$(this).find('input').removeClass('zeroProduto')
		// 		validar = true;
		// 	}
		// 	return validar;
		// });
		// if (validar) {
			alertGlobal("Deseja corfirmar e fechar?", "vazio", "", "Sim", "Não", "opc", "confirma_imagens_upload");
		// }else{

		// }		

	});
});

var qtd = 0;
function readImage(input) {
	if ( input.files && input.files[0] ) {
		var FR= new FileReader();
		FR.onload = function(e) {
			$(".baixoUpload").append('<div class="imgUploadint"><div class="imagemUpl">' +
					'<a class="expandir_IMG" href="' + e.target.result + '"><img class="tmimgupl1" src="' + e.target.result + '" alt="imagem"/></a></div>' +         		    	
					'<div class="div-input-falso cadSlide"><input placeholder="Digite o texto"name="file-falso" type="text"  value=""/></div>' + 
					'<a class="removeupload1"><div class="closeUpl" >+</div></a></div>');
			$(".removeupload1").bind('click',Excluir);
			// $('.expandir_IMG').off("click");
			$('.expandir_IMG').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				closeBtnInside: false,
				fixedContentPos: true,
				mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
				image: {
					verticalFit: true
				},
				zoom: {
					enabled: true,
					duration: 300 // don't foget to change the duration also in CSS
				}
			});
		};       
		FR.readAsDataURL( input.files[0] );
	}
}

function Excluir(){
    var par = $(this).parent(); 	    
    par.remove();
	$("#caminImg").val('');
    qtd =  qtd - 1; 
}
function openGaleriaDeshboard(){
	$('.baixoUpload').find('.imgUploadint').each(function(i){
		$(this).prop('id', 'anexo_Img_'+i+'');
		$('.galeria').append('<aside class="galeriaDesh">'+
							  	'<img src="'+$("#anexo_Img_"+i+" .imagemUpl a img").attr('src')+'" name="'+$("#anexo_Img_"+i+" input").val()+'">'+
								'<a class="exclImgGl">'+
							  		'<span>'+
							  			'+'+
							  		'</span>'+	
							  	'</a>'+
								'<div class="inputGaleria">'+
									'<input value="'+$("#anexo_Img_"+i+" input").val()+'">'+
								'</div>'+
						  	'</aside>');
	});
	$(".baixoUpload").html("");
	$(".exclImgGl").bind('click', ExcluirImgGl);

}
function ExcluirImgGl(){
	var rem = $(this). parent();
		rem.remove();
}

function validarImgs(){
	// alert($(".galeriaDesh").length);
	if($(".galeriaDesh").length){
		JsonImgs();
	}else{
		alertGlobal("Campo Foto Obrigatório","new_Categoria","","FECHAR");
	}
	
}
function JsonImgs(){
	var imgs = {};
		if($(".cimaDir").prop("id") != "" && $(".titulo").prop("id") != undefined){
			imgs._id = $(".cimaDir").prop("id");
		}else{
			// produtos.refbusca = $("#proRef").attr('name');
		}
		imgs.imagens = [];
		$('.galeriaDesh').each(function(i){
			var img = {};
			img.anexo 	= $(this).find('img').prop('src');
			img.nome 	= $(this).find('div input').val();
			imgs.imagens.push(img);
		});

// console.log(JSON.stringify(imgs));
if(imgs.imagens.length>5){
	alertGlobal("Número Maximo de Imagens: 5","" , "","FECHAR");
}else{
	salvarImgs(JSON.stringify(imgs));
	}
}
function salvarImgs(param){
	// alert("salvar as fotos");
	$.ajax({
	 	type: 'POST',
		contentType: "application/json",
		url: "/distribuidores",
		data: param,
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_cliente_bcStore')).token},
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			alertGlobal("Salvo com Sucesso!","","","FECHAR");
			$("#loading").remove();
			limparCampos();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
function busImgsServ(){
	$.ajax({
		type: 'GET',
		contentType: "application/json",
		url: "/servico/buscar",
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_painel_bcStore')).token},
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			// $(".galeria").html("");
			if(data.length>0){
				$(".cimaDir").prop("id",data[0]._id)
			
				$.each(data[0].imagens, function(key){		 			
					$(".galeria").append(
						'<aside class="galeriaDesh">'+
							'<img src="'+data[0].imagens[key].anexo+'" name="'+data[0].imagens[key].nome+'">'+
							'<a class="exclImgGl"><span>+</span></a>'+
							'<div class="inputGaleria">'+
								'<input value="'+data[0].imagens[key].nome+'">'+
							'</div>'+
						'</aside>'
					);
					// function ExcluirImgGl(){
					// var rem = $(this). parent();
					// rem.remove();
					// }
					$(".exclImgGl").bind('click', ExcluirImgGl);
				});
			}
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
function limparCampos(){
	$(".galeria").html("");
}
function openUpload() {
	$("#openUpload").animate({
		"top": "0%"
	}, 500);
}

function offUpload() {
	$("#openUpload").animate({
		"top": "-100%"
	}, 500);
	$('.btnCarregarFoto').removeClass('upl_Ativo');
}