$(function() {
	var infoBusca = {};
	infoBusca.nome = $('#nomeInput').val();
	infoBusca.email = $('#emailInput').val();
	infoBusca.telefone = $('#telefoneInput').val();
	infoBusca.categoria = $('#categoriaInput').val();
	infoBusca.data_cad_inicio = desconverterData($('#inicio').val());
	infoBusca.data_cad_fim = desconverterData($('#fim').val());
	pesquisar(JSON.stringify(infoBusca));
	$('#propostasEnv').notify(
		"Por Favor, Verificar propostas enviadas imediatamente!", 
		{ position:"top center" },
	);
});
$("#file-original").off();
$(document).on('change', '#file-original', function(){
	readImage( this );
})

var qtd = 0;
function readImage(input) {
	if ( input.files && input.files[0] ) {
		var FR= new FileReader();
		FR.onload = function(e) {
			// console.log(e)
			$(".baixoUploadOrcam").append(
			'<aside class="galeriaDesh">'+
			  	// '<img src="' + e.target.result + '"alt="'+$('#file-original').val().split("\\").pop()+'" onerror="this.src=\'../images/quadrado.png\'">'+
			  	'<svg class="icon icon-arquivo"><use xlink:href="#icon-arquivo"></use></svg>'+
			  	'<input value="' + e.target.result + '" type="hidden">'+
			  	'<div class="uploadArquivo">'+
			  		$('#file-original').val().split("\\").pop()+
			  	'</div>'+
			  	'<a class="exclImgGl" style="position: relative;/* right: -22px; *//* left:initial; *//* top: 9px; */display: inline-block;float: left;border-radius: 0px;background: rgba(255, 0, 0, 0.57);width: 19px;height: 19px;box-shadow: 1px 1px 0px black;margin: 8px 5px;">'+
			  		'<span  style="font-size: 20px;">'+
			  			'x'+
			  		'</span>'+	
			  	'</a>'+
			  	'<button id="buttonEnvOrcEmail" type="button" onclick="dadosEmail()" class="btn glyphicon glyphicon-envelope" title="Clique para enviar o anexo">Enviar</button>'+
		  	'</aside>');
			$(".exclImgGl").bind('click',Excluir);
			}
		};       
		FR.readAsDataURL( input.files[0] );
	}
function Excluir(){
    var par = $(this).parent(); 	    
    par.remove();
	$("#caminImg").val('');
    qtd =  qtd - 1;
}
$("tbody").on('click', 'tr', function(event) {
	event.preventDefault();
	// console.log(this.id);
	var aux={};
	aux.id=this.id;
	buscarDadosLista(JSON.stringify(aux));
});
$(".pagination").on('click', '.page-link', function(event) {
	// event.preventDefault();
	// console.log(this);
	var aux={};
	aux.inicio=parseInt($(this).text());
	aux.inicio = (aux.inicio - 1)*10;
	buscarDados(JSON.stringify(aux));
	$('.paginacaoAtivada').removeClass('paginacaoAtivada')
	$(this).addClass('paginacaoAtivada')

});
function buscarDadosLista(id){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/orcamento/buscar",
		data: id,
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			modalOrcamento(data);
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
function buscarDados(inicio){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/produto",
		data: inicio,
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			$('tbody').html("")
			$.each(data, function(key){
				$('tbody').append(
					'<tr id="'+this._id+'">'+
						'<td>'+this.nome+'</td>'+
						'<td>'+this.email+'</td>'+
						'<td>'+this.telefone+'</td>'+
						'<td>'+this.categoria+'</td>'+
						'<td>'+converterData(this.data_cad)+'</td>'+
						// '<td>'+this.data_cad.getDate()+'/'+this.data_cad.getMonth()+1+'/'+this.data_cad.getUTCFullYear()+'</td>'+
					'</tr>'
				)
			});
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "/images/global/alert.png","FECHAR");
			$("#loading").remove();
		}
	});
}
function pesquisar(inicio){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/pesquisar",
		data: inicio,
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			// console.log("datas")
			// console.log(data)			
			// console.log(data.Contador)
			$('tbody').html("")
			$.each(data.Data, function(key){
				$('tbody').append(
					'<tr id="'+this._id+'">'+
						'<td>'+this.nome+'</td>'+
						'<td>'+this.email+'</td>'+
						'<td>'+
							'<input class="telefone" value="'+this.telefone+'" readonly="true">'+
						'</td>'+
						'<td>'+this.categoria+'</td>'+
						'<td class="dataCad">'+converterData(this.data_cad)+'</td>'+
					'</tr>'
				)
				// console.log(this)
				if($('input:radio[name=options]:checked').val() == "finalizado"){

				}else if(this.proposta == "Proposta Enviada"){
					var tdProposta = $("#"+this._id).find(".dataCad");
					tdProposta.html(tdProposta.text()+'<p class="esqCorProposta">Proposta Enviada</p>')
				}
			});
			$(".telefone").mask("(99) 99999-999?9");
			$('.pagination').html('')
			// console.log('Contadorrr')
			// console.log(data.Contador)
			var qntd = data.Contador;
			var resto = qntd%10;
			var buscarPropEnv = "";
			if(JSON.parse(inicio).proposta){
				buscarPropEnv = "propostasEnv";
			}
			if(resto){
				for(var i=1; i <= parseInt(qntd/10)+1; i++ ){
					$('.pagination').append(
						'<li class="page-item">'+
							'<a class="buscarRef '+buscarPropEnv+'">'+i+'</a>'+
						'</li>'
					)
				}
			}else{
				for(var i=1; i<=qntd/10; i++ ){
					$('.pagination').append(
						'<li class="page-item">'+
							'<a class="buscarRef '+buscarPropEnv+'">'+i+'</a>'+
						'</li>'
					)					
				}
			}
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
function buscarHistorico(param){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/historico",
		data: param,
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			// $('tbody').html("")
			$('.modalOrcamento').append(
				'<div class="history">'+
				'<button type="button" class="btn btn-danger btn-block botaoFechar"style="right: 10px;top: 8px;padding: 5px;" title="Clique para fechar">X</button>'+
					'<table class="table table-bordered tableHistorico" style="margin: 1% auto;width: 90%;">'+
						'<thead>'+
						    '<tr>'+
						      '<th>Data</th>'+
						      '<th>Situação</th>'+
						      '<th>Descrição</th>'+
						      '<th>Nome Admin</th>'+
						    '</tr>'+
					   '</thead>'+
					   '<tbody>'+
					   '</tbody>'+
					'</table>'+
				'</div>'
			)
			$.each(data, function(key){
				$('.tableHistorico tbody').append(					
					'<tr>'+
						'<td>'+converterData(data[key].data_cad)+
						'</td>'+
						'<td>'+data[key].situacao+
						'</td>'+
						'<td>'+data[key].descricao+
						'</td>'+						
						'<td>'+data[key].nomeAdmin+
						'</td>'+
					'</tr>'
				)
			});
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alerbtGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
// function buscarPropEnviadas(){
// 	$.ajax({
// 		type: 'POST',
// 		contentType: "application/json",
// 		url: "/proposta",
// 		// data: param,
// 		beforeSend: function(){
// 		},
// 		success: function(data, textStatus, jqXHR, Exception){
// 			console.log(data)
// 		},
// 		error: function(jqXHR, textStatus){
// 			// alertGlobal("Tente novamente!","" , "/images/global/alert.png","FECHAR");
// 		}
// 	});
// }
function dadosEmail(){
	var dados = {};
	dados.nome = $('#tdNome').text();
	dados.email = $('#tdEmail').text();
	dados.anexo = $(".galeriaDesh input").val();
	dados.nomeArq = $('.uploadArquivo').text();
	// console.log(dados)
	enviarEmail(JSON.stringify(dados));

}
function enviarEmail(param){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/email",
		data: param,
		beforeSend: function(){
			if(loading()){
				jsonOrcamento()
			}
		},
		success: function(data, textStatus, jqXHR, Exception){
			alertGlobal("Email enviado com sucesso!","" , "","FECHAR");
			$("#enviandoEmail").remove();
			$("#loading").remove();

		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
function modalOrcamento(dados){
	$("body").append(
		'<section id="'+dados._id+'" class="modalOrcamento">'+
			'<div class="divModal">'+
				'<button type="button" class="btn btn-danger btn-block botaoFechar" title="Clique para fechar">X</button>'+
				'<table class="table table-bordered" style="width: 90%;margin: 1% auto;">'+
				  '<thead>'+
				    '<tr>'+
				      '<th>Nome</th>'+
				      '<th>Email</th>'+
				      '<th>Telefone</th>'+
				      '<th>Categoria</th>'+
				    '</tr>'+
				  '</thead>'+
				  '<tbody>'+
				    '<tr>'+
				      '<td id="tdNome">'+dados.nome+'</td>'+
				      '<td id="tdEmail">'+dados.email+'</td>'+
				      '<td>'+dados.telefone+'</td>'+
				      '<td>'+dados.categoria+'</td>'+
				    '</tr>'+
				  '</tbody>'+
				'</table>'+
				'<table class="table table-bordered" style="width: 90%;margin: 1% auto;">'+
				  '<thead>'+
				    '<tr>'+
				      '<th>Nome do Produto</th>'+
				      '<th>Descrição</th>'+
				      '<th class="qntPro">Quantidade</th>'+
				      '<th>Anexos</th>'+
				      '<th>Situação</th>'+
				      '<th>Descrição Admin</th>'+
				      '<th>Histórico</th>'+
				    '</tr>'+
				  '</thead>'+
				  '<tbody class="trProdutos">'+
				  '</tbody>'+	
				'</table>'+
				'<row style="position: relative;display: inline-block;width: 100%;">'+
					'<div class="buttonSalvar col-sm-6">'+
						'<button id="salvarSituacao" type="button" class="btn btn-success" title="Clique para salvar a situação e descrição deste orçamento">SALVAR</button>'+
					'</div>'+
					'<div class="buttonEnviarOrcam col-sm-6" style="display: none;">'+
						'<a class="btn-primary">'+
							'<span class="glyphicon glyphicon-paperclip"></span>ANEXO'+
								'<input class="enviarOrcam" type="file" name="file-original" id="file-original" title="Clique para anexar o arquivo com o orçamento, depois clique no botão enviar">'+
								'</input>'+
					'</div>'+
					'<div class="baixoUploadOrcam">'+
					'</div>'+
				'</div>'+
		'</section>'
	)
	$.each(dados.produtos, function(key){
		// console.log(dados)
		$(".trProdutos").append(
		  '<tr id="'+dados.produtos[key]._id+'">'+
			  '<td>'+dados.produtos[key].nomePro+'</td>'+
			  '<td>'+dados.produtos[key].descricaoPro+'</td>'+
		      '<td class="qntPro">'+dados.produtos[key].qtdePro+'</td>'+
		      '<td class="tdFotos'+key+'" title="Clique para visualizar o anexo">'+
		      	// ''<div class="tdImg" style="background: url('+dados.produtos[key].fotos[0].fotosPro+') no-repeat;width: 40px;height: 30px;background-size: 100%;"></div>'+
		   	  '</td>'+
		   	  '<td class"tdSituacao">'+
			   	  '<select class="opSituacao" title="Clique e selecione a situação em que se encontra este orçamento">'+
				  '</select>'+
		   	  '</td>'+
		   	  '<td>'+
					'<input class="descAdm" type="text" name="descAdm" value="" placeholder="Digite uma descrição para situação" title="Digite uma descrição a respeito da situação deste orçamento">'+
					// '<input class="descAdm" type="text" name="descAdm" value="'+dados.produtos[key].infoAdm[dados.produtos[key].infoAdm.length-1].descricao+'">'+
		   	  '</td>'+
		   	  '<td>'+
		   	  		'<button type="button" id="'+key+'" class="btn btn-primary btn-historico glyphicon glyphicon-list-alt"style="margin: auto;display: block;" title="Clique para visualizar o histórico completo deste orçamento"></button>'+
		   	  '</td>'+
	   	  '</tr>'
		)
		if(dados.proposta == "Proposta Enviada"){
			$("#"+dados.produtos[key]._id+" .opSituacao").append(
				  '<option value="finalizadoAp">Finalizado Aprovado</option>'+
				  '<option value="finalizadoNaoAp">Finalizado Não Aprovado</option>'
			)
			$('.buttonEnviarOrcam').css({display : 'none'});
		}else{
			$("#"+dados.produtos[key]._id+" .opSituacao").append(
				  '<option value="">Escolha uma opção</option>'+
				  '<option value="prospEnv" class="prospEnv">Proposta Enviada</option>'+
				  '<option value="finalizado">Finalizado Sem Contato</option>'+
				  '<option value="aguardRetornoInfo">Aguardando Retorno Informação</option>'
			)
			if(dados.produtos[key].infoAdm.length){
				$("#"+dados.produtos[key]._id+" .descAdm").val(dados.produtos[key].infoAdm[dados.produtos[key].infoAdm.length-1].descricao)
			}
			$(".opSituacao").on('change', function(e){
				if($(".opSituacao").val()=="prospEnv"){
					$("#salvarSituacao").attr('disabled', 'disabled');
					$('.buttonEnviarOrcam').css({display : 'block'});
				  // alert($(".opSituacao").val())
				}else{
					$("#salvarSituacao").removeAttr('disabled')
					$(".buttonEnviarOrcam").css({display : 'none'})
					$(".galeriaDesh").remove();
				}
			});
		}
		if(dados.produtos[key].infoAdm.length){
			$('#'+dados.produtos[key]._id+' .opSituacao').val(dados.produtos[key].infoAdm[dados.produtos[key].infoAdm.length-1].situacao)
			var situacaoTemp = (dados.produtos[key].infoAdm[dados.produtos[key].infoAdm.length-1].situacao);
			if(situacaoTemp =='finalizado' || situacaoTemp == 'finalizadoAp' || situacaoTemp == 'finalizadoNaoAp'){
				$('#'+dados.produtos[key]._id+' .opSituacao').attr({
					disabled: 'disabled'
				}).css({
					background : '#c8c6c6'
				});
				$('#'+dados.produtos[key]._id+' .descAdm').attr({
					disabled: 'disabled'
				}).css({
					background : '#c8c6c6'
				});
			}		
		}
		if(dados.categoria == 'produto'){
			$.each(dados.produtos[key].fotos, function(chave){
				$(".tdFotos"+key).append(
					'<div class="tdImg">'+
			    		'<div style="background: url('+dados.produtos[key].fotos[chave].fotosPro+') no-repeat;width: 40px;height: 30px;background-size: 100%;"></div>'+
					'</div>'
				)
			});	
		}else{
			$.each(dados.produtos[key].fotos, function(chave){
				// console.log(dados.produtos[key].fotos[chave].fotosNome)
				$(".tdFotos"+key).append(
					'<a class="anexo">'+dados.produtos[key].fotos[chave].fotosNome+
			    		'<input readonly class="tdImg" type="hidden" value="'+dados.produtos[key].fotos[chave].fotosPro+'">'+
						// '<div >'+dados.produtos[key].fotos[chave].fotosPro+
						// '</div>'+
					'</a>'
				)
			});	
			$('.qntPro').css({
				display : 'none',
			});

		}
	});

}
function jsonOrcamento(){
	var orcamento = {};
	orcamento._id = $('.modalOrcamento').attr('id');
	orcamento.produtos= [];
	orcamento.estadoPed = "finalizado";
	$.each($('.trProdutos tr'), function(chave){
		var pro = {};
		pro._id = this.id;
		pro.situacao = $(this).find('.opSituacao').val();
		pro.descricao = $(this).find('.descAdm').val();
		pro.nomeAdmin = $(".logout").attr("name")
		pro.id_Admin = $(".logout").attr("id")
		orcamento.produtos.push(pro);
		if(!($('.opSituacao').val()=='finalizado' || $('.opSituacao').val()=='finalizadoAp' || $('.opSituacao').val()=='finalizadoNaoAp'))
		{
			orcamento.estadoPed = "ativo";
		}
	});
	if($(".galeriaDesh input").length){
		orcamento.anexoOrc = $(".galeriaDesh input").val();
	}
	if($('.opSituacao').val()=="" || $('.opSituacao').val()==null){
		alertGlobal("Por favor, preencher campo situação!");
	}
	// else if($('.opSituacao').val()=='Proposta Enviada'){
	// 	if($(".galeriaDesh input").length==0){
	// 		alertGlobal("Por favor, preencher campo situação!");
	// 	}
	// }
	else{
		salvarSituacao(JSON.stringify(orcamento))
	}
}
$('body').on('click', '#salvarSituacao', function(event) {
	jsonOrcamento()
});
function salvarSituacao(orcamento){
	var tela = false;

	if($("#loading").length){
		tela=true;
		console.log('tela ja existe')
	}
	$.ajax({
	 	type: 'POST',
		contentType: "application/json",
		url: "/situacao",
		data: orcamento,
		// headers: {'x-access-token': $.parseJSON(localStorage.getItem('token_cliente_bcStore')).token},
		beforeSend: function(){
			if(!tela){
				loading();
			}
		},
		success: function(data, textStatus, jqXHR, Exception){
			$(".modalOrcamento").remove();
			if(!tela){
				alertGlobal(data.message,"","","FECHAR");
				$("#loading").remove();
			}
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});

}
$('body').on('click', '.botaoFechar', function(event) {
	// event.preventDefault();
	/* Act on the event */
	$('.modalOrcamento').remove()
});
$('body').on('click', '.anexo', function(event) {
	var aux = $(this).find("input").val();
	// alert('entrou')
	$('body').append(
		'<section id="infoAnexo">'+
			'<button type="button" class="btn btn-danger btn-block botaoFecharAnexo" title="Clique para fechar o anexo">X</button>'+
			'<iframe src="'+aux+'">'+
			'</iframe>'+
		'</section>'
	)
	$(".botaoFecharAnexo").click(function(event) {
		// alert('clico remove')
		$("#infoAnexo").remove();
	});
	$("body").keydown(function(e) {
		if(e.which == 27) {
	    $("#infoAnexo").remove();
		}
	});
});
$('body').on('click', '.tdImg', function(event) {
	var aux = $(this).html();
		$('body').append(
			'<section id="imgGrande">'+
				'<div class="telaGrande" title="Clique para fechar a imagem">'+aux+'</div>'+
			'</section>'
		)
	$("#imgGrande").on('click', function(event) {
		$("#imgGrande").remove();
	});
	$("body").keydown(function(e) {
		if(e.which == 27) {
	    $("#imgGrande").remove();
		}
	});
	});
	$('body').on('click', '.buscarRef', function(event) {
		if($(this).text()=="PROPOSTAS ENVIADAS"){
			$("#option1").prop('checked',true)
			$('.btnRadios').removeClass("btnRadios")
			$('input:radio[name=options]:checked').parent().addClass('btnRadios')
		}
		BuscaRefinada(this);
	});
	function BuscaRefinada(localClick) {
		var infoBusca = {};
		if($(localClick).text() && $(localClick).text()!="PROPOSTAS ENVIADAS" ){
			infoBusca.inicio = parseInt($(localClick).text());
			infoBusca.inicio = (infoBusca.inicio - 1)*10;
		}else if($(localClick).text()=="PROPOSTAS ENVIADAS"){
			infoBusca.proposta = "Proposta Enviada";
		}
		if($(localClick).hasClass('propostasEnv')){
			infoBusca.proposta = "Proposta Enviada";
		}
		infoBusca.estadoPed = $('input:radio[name=options]:checked').val();
		infoBusca.nome = $('#nomeInput').val();
		infoBusca.email = $('#emailInput').val();
		infoBusca.telefone = $('#telefoneInput').val();
		infoBusca.categoria = $('#categoriaInput').val();
		infoBusca.data_cad_inicio = desconverterData($('#inicio').val());
		infoBusca.data_cad_fim = desconverterData($('#fim').val());
		pesquisar(JSON.stringify(infoBusca));
	}
	$('input:radio[name=options]').change(function(event) {
		var infoBusca = {};
		infoBusca.estadoPed = $('input:radio[name=options]:checked').val();
		$('input[type=text]').val("")
		pesquisar(JSON.stringify(infoBusca));
		$('.btnRadios').removeClass("btnRadios")
		$('input:radio[name=options]:checked').parent().addClass('btnRadios')
	});
function converterData(e){
	var d = new Date(e)
	var dia = addZero(d.getDate());
	var mes = addZero(d.getMonth()+1);
	var ano = d.getUTCFullYear();
	return ''+dia+'/'+mes+'/'+ano;
}
function desconverterData(e){
	var dia = e.slice(0,2);
	var mes = e.slice(3,5);
	var ano = e.slice(6,10);
	return ''+ano+'-'+mes+'-'+dia;
}
function addZero(num){
	// console.log(num)
	var texto;
	texto = "";
	if(num<10) {
		texto = '0'+num;
	}else{
		texto = num;
	}
	// console.log(texto)
	return texto;
}
$('#datepicker').datepicker({
	format: "dd/mm/yyyy",
	language: 'pt-BR',
	autoclose: true
});
$('body').on('click', '.trProdutos button', function(event) {
	var parametros = {};
	parametros.id = $('.modalOrcamento').attr('id');
	// parametros.id_produto = $(this).parent().parent().attr('id');
	parametros.posicao = this.id;
	buscarHistorico(JSON.stringify(parametros));
});
// $('body').on('click', '#propostasEnv', function(event) {
// 	// buscarPropEnviadas();
// });
$(".telefone").mask("(99) 99999-999?9");