$("#file-original").off();
$(document).on('change', '#file-original', function(){	
		readImage( this );
});

var qtd = 0;
function readImage(input) {
	if ( input.files && input.files[0] ) {
		var FR= new FileReader();
		FR.onload = function(e) {
			$(".baixoUpload").html("");
			$(".baixoUpload").append(
			'<aside class="galeriaDesh">'+
				  	'<img src="' + e.target.result + '">'+
				  	'<a class="exclImgGl">'+
				  		'<span>'+
				  			'x'+
				  		'</span>'+	
				  	'</a>'+
			  	'</aside>');
			$("#imagem").val(e.target.result);
			$(".exclImgGl").bind('click',Excluir);
			}
		};
		FR.readAsDataURL( input.files[0] );
}
function Excluir(){
    var par = $(this).parent(); 	    
    par.remove();
	$("#caminImg").val('');
    qtd =  qtd - 1;
}
$("input.telefone_parceiros").mask("(99) 99999-999?9");
$("input.cep_parceiros").mask("99999-999");
document.querySelector(".email_parceiros").addEventListener("change", function(){
	var pegar_email = document.querySelector(".btn_editar_parceiros");
	pegar_email.href = "/parceiros/edit/"+this.value;
});