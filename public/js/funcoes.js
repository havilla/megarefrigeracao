//Mover Menu suave
$(function() {
	$('#navbar li a, .bottom_ul li a').click(function(event) {
		moverSuave($("#"+$(this).attr('name')+"").offset().top);
	});
})

function moverSuave(param){
   	$("body,html").animate({ scrollTop:param}, 2000);
}

//Clientes - aumentando imagem
// $("#clientes").on('click', 'img', function(event) {
// 	// alert ($(this).attr("name"))
// 	modalImg($(this).attr("name"))
// });
// function modalImg(idImg) {
// $("body").append('<section id="alertGlobal" class="wrapAlert container-fruid"> ' +
// 	'<div class="corpAlert row">' +
// 	'<div class="col-sm-12">'+
// 	'<div class="Descricao"<h1>Aqui vai informação da empresa</h1>'+
// 	'<img class="imgAtencao img-thumbnail" src="./images/clientes/'+idImg+'Grande.png">' +
// 	'</div>' +
// 	'</section>');
// $("#alertGlobal").on('click', function(event) {
// 	$("#alertGlobal").remove();
// });
// $("body").keydown(function(e) {
// 	if(e.which == 27) {
//     $("#alertGlobal").remove();
// 	}
// });
// }
var focusAlert;
// function alertGlobal(img, msg, id, nomeBt){
// function alertGlobal(msg, id, img){
function alertGlobal(msg, focus, img, nom1btn, nom2btn, opc, parm) {
	focusAlert = focus;
	if (opc == undefined) {
		$("body").append(
			'<section class="wrap">' +
				'<aside class="corpo">' +
					'<aside class="before">' +
						'<span class="imgBc ' + img + '"></span>' +
						'<span>' + msg + '</span>' +
						'<button class="btn_Alert">FECHAR</button>' +
					'</aside>' +
				'</aside>' +
			'</section>');
		$('.btn_Alert').bind('click', excluirAlertGolbal);
	} else {
		$("body").append(
			'<section class="wrap">' +
				'<aside class="corpo">' +
					'<aside class="before">' +
						'<span class="imgBc ' + img + '"></span>' +
						'<span>' + msg + '</span>' +
						'<button class="fecharSim">' + nom1btn + '</button>' +
						'<button class="fecharNao">' + nom2btn + '</button>' +
					'</aside>' +
				'</aside>' +
			'</section>');
	}
	$(".fecharSim").bind('click', function() {
		if (parm == "confirma_imagens_upload") {
			openGaleriaDeshboard();
			offUpload();
		} else if (parm == "limparDetalhes") {
			$(".textParag, .titDestq").remove();
			$("#titDestq").text("");
			$(".offMaisDetalhes").removeClass('offMaisDetalhes_oculto');
			$(".maisDetalhes").removeClass('maisDetalhes_ativo');
			offMaisDetalhes();
		} else if (parm == "mudaritem") {
			$(".offMaisDetalhes").unbind();
			$(".textParag, .titDestq").remove();
			$("#titDestq").text("");
			$(".offMaisDetalhes").removeClass('offMaisDetalhes_oculto');
			$(".maisDetalhes").removeClass('maisDetalhes_ativo');
			offMaisDetalhes();

			$(".itemMenu").removeClass('iteMenu_ativo')
			$(this).addClass('iteMenu_ativo');
			openMenu(opc);
		} else if (parm == "categoria") {
			jsonCategoria();
		} else if (parm == "exCcategoria") {
			if ($("#sl_Categoria option:selected").val() != "") {
				excCategori($("#sl_Categoria option:selected").val());
			}
		} else if (parm == "loginCli") {			// $(".headerUser").remove();
			// localStorage.removeItem("token_painelCli_bcstore");
			// $("#login").css('display', 'initial');
			$("aside.login").removeClass('logado')
			window.localStorage.removeItem('token_painelCli_bcstore');
			window.localStorage.removeItem('tbProdutos');
			$(".logar").html(
				'<a class="">Faça login ou cadastre-se.</a>');
		}
	});

	$(".fecharSim, .fecharNao").bind('click', excluirAlertGolbal);
}
function excluirAlertGolbal() {
	var rem = $(this).parent().parent().parent();
	rem.remove();
	// $("#" + focusAlert).focus();
}
$('#manutencao').on('click', '.btn-link', function(event) {
	// pega o 'name' do botão onde clicou e salva na variavel
	var localIframe = $(this).attr('name');
	// usa a variavel para concatenar no src para abrir o iframe no local desejado
	$('#manutencao .telaManutencao').append(
		'<iframe id="frame-spec" src=../html/manutencao.html#'+localIframe+' name="janela" scrolling="no"></iframe>'
		);
});

//Manutencao - aumentando imagem
function modalImg(liImg) {
	var srcImg = liImg.querySelector("img").src;
	var srcGrande = srcImg.slice(srcImg.search('images'), srcImg.search('.png'))+"Grande.png";
	$("body").append('<section id="alertGlobal" class="wrapAlert container-fruid"> ' +
		'<div class="corpAlert row">' +
		'<div class="col-sm-12">'+
		'<img class="imgAtencao img-thumbnail" src="../'+srcGrande+'" title="Clique para fechar a imagem">' +
		'</div>' +
		'</section>');
	$("#alertGlobal").on('click', function(event) {
		$("#alertGlobal").remove();
	});
	$("body").keydown(function(e) {
		if(e.which == 27) {
	    $("#alertGlobal").remove();
		}
	});
}

function loading(){
	$('body').append(
		'<section id="loading">'+
			'<div class="divLoading">'+
				'<img class="imgLoading" src="../images/bx_loader.gif">'+
			'</div>'+
		'</section>'
	)
	$("#loading").addClass('loadingColor');
	return true;
}
$('#navbar li a, section, #btnOrcamento2').click(function(event) {
	$('#botaoMenu').addClass('collapsed')
	$('#navbar').removeClass('in')
	$('#botaoMenu').attr("aria-expanded", false)
});
// $('section').click(function(event) {
// 	$('#botaoMenu').addClass('collapsed')
// 	$('#navbar').removeClass('in')
// 	$('#botaoMenu').attr("aria-expanded", false)
// });
