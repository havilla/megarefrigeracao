document.querySelector("#ufCidade").addEventListener('change', buscarParceiros)

function buscarParceiros(argument) {
	var info = {}
	info.estado = document.querySelector("#ufCad").value;
	info.cidade = document.querySelector("#ufCidade").value;
	ajaxbuscarparceiros(JSON.stringify(info));
}
function ajaxbuscarparceiros(param){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/parceiros/buscar",
		data: param,
		beforeSend: function(){
		},
		success: function(data, textStatus, jqXHR, Exception){
			exibir_lista(data.Data);
			// alertGlobal("Parceiro Cadastrado com Sucesso!","FECHAR");
		},
		error: function(jqXHR, textStatus){
			// alertGlobal("Tente novamente!","" , "","FECHAR");
		}
	});
}
var nome_Empresa = [];
var info_marker = [];
function exibir_lista(data){
	document.querySelector("#lista_parceiros ul").innerHTML="";
	var ul = document.querySelector("#lista_parceiros ul");
	data.forEach( function(element, index) {
		var li = document.createElement("li");
		li.setAttribute("id",element._id);
		var container_texto = document.createElement("div")
		container_texto.className = "container_text"
		var nomeEmpresa = document.createElement("div");
		nomeEmpresa.textContent = element.nomeEmpresa;
		nomeEmpresa.className = "tituloNot btn-detalhe";
		nomeEmpresa.addEventListener('click', buscarParceiroId)
		container_texto.appendChild(nomeEmpresa)
		var logradouro = document.createElement("p");
		logradouro.textContent = element.endereco[0].logradouro+', '+element.endereco[0].complemento;
		container_texto.appendChild(logradouro)
		var bairro = document.createElement("p");	
		bairro.textContent = element.endereco[0].bairro;
		container_texto.appendChild(bairro)
		var cidade = document.createElement("p");	
		cidade.textContent = element.cidade+' - '+element.estado;
		container_texto.appendChild(cidade)
		// var estado = document.createElement("spam");	
		// estado.textContent = element.estado;
		// container_texto.appendChild(estado)		
		var telefone = document.createElement("p");
		telefone.textContent = 'Telefone: '+element.telefone;
		container_texto.appendChild(telefone)
		var detalhes = document.createElement("div")
		detalhes.className = "btn-detalhe detalhes";
		detalhes.textContent = 'Detalhes'
		detalhes.addEventListener('click', buscarParceiroId)
		container_texto.appendChild(detalhes)
		var link1 = document.createElement("input")
		link1.className = "latitude";
		link1.style.display = 'none';
		link1.value = element.latitude;
		container_texto.appendChild(link1)
		var link2 = document.createElement("input")
		link2.className = "longitude";
		link2.style.display = 'none';
		link2.value = element.longitude;
		container_texto.appendChild(link2)
		li.addEventListener('click', buscarMapa)
		li.appendChild(container_texto)
		// var copia = JSON.parse(li+" ");
		// console.log('copia')
		// console.log(copia)
		ul.appendChild(li)
		locations.push({lat: parseFloat(element.latitude), lng: parseFloat(element.longitude), index})
		
		nome_Empresa.push(element.nomeEmpresa);
		info_marker.push(element);
	});
	document.querySelector("#lista_parceiros").appendChild(ul)
}
function buscarMapa(){
	if(document.querySelector(".ativo")){
		document.querySelector(".ativo").className = "";
	}
	this.className = "ativo";
	
	zoom = 18;
	lat = parseFloat(this.querySelector(".latitude").value);
	lng = parseFloat(this.querySelector(".longitude").value);
	uluru = {lat, lng};
	initMap();
}
//API Google Maps
var zoom = 10;
var lat = -16.6806193;
var lng = -49.2585262;
function initMap() {
var map = new google.maps.Map(document.getElementById('map'), {
zoom: zoom,
center: {lat: lat, lng: lng}
});
var marker = new google.maps.Marker({});
// var contentString = '<div id="content">'+
//             '<div id="siteNotice">'+
//             '</div>'+
//             '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
//             '<div id="bodyContent">'+
//             '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
//             'sandstone rock formation in the southern part of the '+
//             'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
//             'south west of the nearest large town, Alice Springs; 450&#160;km '+
//             '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
//             'features of the Uluru - Kata Tjuta National Park. Uluru is '+
//             'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
//             'Aboriginal people of the area. It has many springs, waterholes, '+
//             'rock caves and ancient paintings. Uluru is listed as a World '+
//             'Heritage Site.</p>'+
//             '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
//             'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
//             '(last visited June 22, 2009).</p>'+
//             '</div>'+
//             '</div>';

//         var infowindow = new google.maps.InfoWindow({
//           content: contentString,
//           maxWidth: 200
//         });
var contentString = "";
function montar_janela_info(dados){
	var temp = '<div id="content" class="row">'+
            '<div class="col-sm-3 foto_info">'+
            	'<img class="foto_parceiro img-thumbnail"src="'+dados.foto+'">'+
            '</div>'+
            '<div class="col-sm-9">'+
	           	'<div id="bodyContent">'+
            	'<h1 id="firstHeading" class="firstHeading">'+dados.nomeEmpresa+'</h1>'+
	            '<p>'+dados.endereco[0].logradouro+', '+dados.endereco[0].complemento+'</p>'+
	            '<p>'+dados.endereco[0].bairro+'</p>'+
	            '<p>'+dados.cidade+' - '+dados.estado+'</p>'+
	            '<p>'+'Telefone: '+dados.telefone+'</p>'+
            '</div>'+
            '</div>'+
            '</div>';
    return temp;
}
locations.forEach( function(element, index) {
		var infowindow = new google.maps.InfoWindow({
	      content: montar_janela_info(info_marker[index]),
	      maxWidth: 400
	    });
        var marker = new google.maps.Marker({
          position: element,
          map: map,
          title: nome_Empresa[index]
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
          console.log(this)
        });
});
// Create an array of alphabetical characters used to label the markers.
// var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
// var markers = locations.map(function(location, i) {
// return new google.maps.Marker({
// position: location,
// label: labels[i % labels.length]
// });
// });
// Add a marker clusterer to manage the markers.
var markerCluster = new MarkerClusterer(map, marker,
{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
return map;
}
var locations = [
]
function buscarParceiroId(){
	ajaxmodalparceiros(this.parentNode.parentNode.id);
	// ajaxmodalparceiros();	
}
function ajaxmodalparceiros(param){
	// alert('entrou')
	console.log(param)
	$.ajax({
		type: 'GET',
		contentType: "application/json",
		url: "/parceiros/buscar/"+param,
		data: param,
		beforeSend: function(){
		},
		success: function(data, textStatus, jqXHR, Exception){
			// alertGlobal("Email Enviado Com Sucesso!","FECHAR");
			detalhes_Parceiros(data.Data);
			// console.log(data)
		},
		error: function(jqXHR, textStatus){
			// alertGlobal("Tente novamente!","" , "","FECHAR");
		}
	});
}

function detalhes_Parceiros(dados){
	// console.log(dados); 
	var modal = document.querySelector(".modalParceiros");
	var img_parceiro = document.querySelector("#name_foto img")
	img_parceiro.className = "img-thumbnail img_parceiros_modal"
	img_parceiro.src = dados.foto;
	var nomeEmpresa = document.querySelector("#name_empresa");
	nomeEmpresa.textContent = dados.nomeEmpresa;	
	var nomeProp = document.querySelector("#name_proprietario");
	nomeProp.textContent = dados.nome;
	var email = document.querySelector("#name_email");
	email.textContent = dados.email;
	var telefone = document.querySelector("#name_telefone");
	telefone.textContent = dados.telefone;	
	var descricao = document.querySelector("#name_descricao");
	descricao.textContent = dados.descricao;	
	var endereco = document.querySelector("#name_endereco");
	var end = dados.endereco[0];
	endereco.textContent = end.logradouro+' '+end.complemento+' '+end.bairro+' '+dados.cidade+' - '+dados.estado+' '+end.cep;

	var button = document.createElement("button");
	button.className = "btn btn-danger btn-parceiros";
	button.addEventListener('click', esconder_modal);
	button.textContent = 'X';
	modal.appendChild(button)
	document.querySelector("#modal_parceiros").style.zIndex = '9';
}
function esconder_modal(){
	document.querySelector("#modal_parceiros").style.zIndex = '-1';
}
