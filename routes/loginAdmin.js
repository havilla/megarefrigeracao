module.exports = function(app){
	var loginAdmin = app.controllers.loginAdmin;
	var autenticar = require('../middleware/autenticarAdmin')
	app.route('/loginAdmin')
		.get(loginAdmin.index)
	app.route('/home')
		.get(autenticar, loginAdmin.home)
		.post(loginAdmin.autenticacao)
	app.route('/usuarios/create')
		.get(autenticar, loginAdmin.create)
		.post(loginAdmin.post);
	app.route('/sair')
		.get(loginAdmin.sair);
	app.route('/emailAdmin')
		.get(autenticar, loginAdmin.emailAdmin)
	app.route('/emailAdmin/create')
		.get(autenticar, loginAdmin.emailCreate)
		.post(loginAdmin.postEmail);
	app.route('/email/excluir/:id')
		.get(autenticar, loginAdmin.show);
	app.route('/email/delete/:id')
		.post(loginAdmin.delete);
}