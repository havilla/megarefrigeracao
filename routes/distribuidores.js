module.exports = function(app){
	var distribuidores = app.controllers.distribuidores;
	var autenticar = require('../middleware/autenticar')

	app.route('/distribuidores')
		.get(autenticar, distribuidores.index)
		.post(distribuidores.salvar);
	app.route('/distribuidores/buscar')
		.get(distribuidores.buscar);
}