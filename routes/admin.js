module.exports = function(app) {
	var admin = app.controllers.admin;
	var autenticar = require('../middleware/autenticarAdmin');
	app.route('/admin')
		.get(admin.login)
	app.route('/salvarAdmin')
		.post(admin.salvar);
	app.route('/login')
		.get(admin.login)
		.post(admin.autenticacao);
	app.route('/logout')
		.get(admin.logout);
	app.route('/usuarios/edit/:id')
		.get(autenticar, admin.edit)
		.post(admin.update);
}