module.exports = function(app){
	var clientes = app.controllers.clientes;
	var autenticar = require('../middleware/autenticar')
	
	app.route('/clientes')
		.get(autenticar, clientes.index)
		.post(clientes.salvar);
	app.route('/clientes/buscar')
		.get(clientes.buscar);
}