module.exports = function(app){
	var personalizar = app.controllers.personalizar;
	var autenticar = require('../middleware/autenticarAdmin')

	var fs = require('fs');
	var path = 'public/stylesheets/css/mylibs/dinamico'
	var path2 = 'views/html'

	var express = require('express');
	var router = express.Router();

	var multer = require('multer');
	var storage = multer.diskStorage({
		destination: function(req, file, cb) {
			cb(null, 'public/images/logo')
		},
		filename: function(req, file, cb) {
			console.log('entrou filename')
			console.log(file)
				var ext = file.originalname.substr(file.originalname.lastIndexOf('.') + 1)
				cb(null, file.originalname)
				// nomeImagem.push(nome)
				// console.log("nomeImagem[0]")
				// console.log(nomeImagem[0])
				// cb(null, file.fieldname + '-' + Date.now()+".jpg")
		}
	})
	var upload = multer({
		storage: storage
	}).array('photos', 12)

	app.route('/personalizar')
		.get(autenticar, personalizar.personalizar);

	// app.route('/salvarLogoMarca')
	// 	.post(function(req, res){	
			// upload(req, res, function(err) {

			// });
	// 	});

	app.route('/salvarBarra')
		.post(function(req, res){
			var logo = "<img class='img-responsive' src="+req.body.src+">";
				fs.writeFile(path2+'/logo.html', logo, function (err) {
					  if (err) throw err;
					  	console.log('It\'s saved Logo!');
					 //  	req.flash('info', 'Registro cadastrado com sucesso!');
						// res.redirect('/personalizar');
				});
			var css =
				'section#barra {'+
				    'background: '+req.body.barra.background+';'+
				'}'+
				'.circle {'+
					'border: 3px solid '+req.body.circle.border+';'+
				'}'+
				'.informacao {'+
					'border-top: 3px solid '+req.body.informacao.borderTop+';'+
					'border-bottom: 3px solid '+req.body.informacao.borderTop+';'+
					'border-right: 3px solid '+req.body.informacao.borderTop+';'+
					'background-color: '+req.body.barra.color+';'+
				'}'+
				'section#barra h4{'+
				'   color: '+req.body.barraFonte.color+';'+
				'}'+
				'section#barra .circle svg{'+
					'color: '+req.body.barraFonte.color+';'+
				'}'+
				'.orcamentoHover:first-child:hover #circleOrc, .orcamentoHover:first-child:hover #bntOrcamento{'+
					'background-color: '+req.body.redeSocialHover.backgroundColor+';'+
				'}'+
				'.Face:hover, .Insta:hover, .Google:hover, .Linkdin:hover {'+
					'background-color: '+req.body.redeSocialHover.backgroundColor+';'+
				'}'+
				'.Social2>div{'+
					'background-color: '+req.body.redeSocial.backgroundColor+';'+
				'}';
			var cssMenu = 
				'div#menuCont {'+
					'background: '+req.body.menu.background+';'+
				'}'+
				'div#menu li h4{'+
					'color: '+req.body.menuFonte.color+';'+
				'}'+
				'.nav > li > a:hover, div#menu .nav > li > a:hover h4{'+
					'color: '+req.body.menuHover.color+';'+
				'}'+
				'.nav > li > a:hover h4{'+
					'border-bottom: 11px solid '+req.body.menuHover.color+';'+
				'}';
			var cssCarrosel =
				'#primary {'+
	    			'background: '+req.body.carrosel.background+';'+
				'}'+
				'#primary .bx-wrapper {'+
					'background: '+req.body.carroselBorda.backgroundBorder+';'+
				'}';
				// console.log(css)
			fs.writeFile(path+'/all2.css', css+cssMenu+cssCarrosel, function (err) {
				  if (err) throw err;
				  	console.log('It\'s saved!Admin2');
					req.flash('info', 'Salvo com sucesso!');
					res.json({success: true, message: 'Salvo com sucesso!'});
		});
	});
}



					// fs.writeFile(path+'/sliderAdmin.html', conteudoAdmin, function (err) {
					//   if (err) throw err;
					//   	console.log('It\'s saved!Admin');
					//   	req.flash('info', 'Registro cadastrado com sucesso!');
					// 		res.redirect('/sliderprinc');
					// });