module.exports = function(app){
	var servico = app.controllers.servico;
	var autenticar = require('../middleware/autenticar')

	app.route('/servico')
		.get(autenticar, servico.index)
		.post(servico.salvar);
	app.route('/servico/buscar')
		.get(servico.buscar);
}