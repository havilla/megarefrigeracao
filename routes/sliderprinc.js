	module.exports = function(app) {
	var sliderprinc = app.controllers.sliderprinc;
	var autenticar = require('../middleware/autenticar')

	app.route('/sliderprinc')
		.get(autenticar, sliderprinc.index)
		.post(autenticar, sliderprinc.salvar);
}