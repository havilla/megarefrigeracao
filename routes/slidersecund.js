module.exports = function(app){
	var slidersecund = app.controllers.slidersecund;
	var autenticar = require('../middleware/autenticar')

	app.route('/slidersecund')
		.get(autenticar, slidersecund.index)
		.post(slidersecund.salvar);
	app.route('/slidersecund/buscar')
		.get(slidersecund.buscar);
}