module.exports = function(app){
	var fotosempresa = app.controllers.fotosempresa;
	var autenticar = require('../middleware/autenticar')

	app.route('/fotosempresa')
		.get(autenticar, fotosempresa.index)
		.post(fotosempresa.salvar);
	app.route('/fotosempresa/buscar')
		.get(fotosempresa.buscar);
}