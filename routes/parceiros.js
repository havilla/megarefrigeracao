module.exports = function(app){
	var parceiros = app.controllers.parceiros;
	var autenticar = require('../middleware/autenticarAdmin')
	
	app.route('/parceiros')
		.get(autenticar, parceiros.parceiros)
	app.route('/parceiros/create')
		.get(autenticar, parceiros.create)
		.post(parceiros.post);
	app.route('/parceiros/buscar')
		.post(parceiros.buscar);
	app.route('/parceiros/buscar/:id')
		.get(parceiros.buscarId);	
	app.route('/parceiros/edit/:email')
		.get(autenticar, parceiros.editar)
		.post(parceiros.update);
}