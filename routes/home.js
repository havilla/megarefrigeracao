module.exports = function(app){
	var home = app.controllers.home;
	var autenticar = require('../middleware/autenticar');
	app.route('/')
		.get(home.index);
	app.route('/orcamento')
		.get(autenticar, home.buscarDados)
		.post(home.salvarOrcamento);
	app.route('/produto')
		.get(home.indexProduto)
		.post(home.buscarDados);
	app.route('/orcamento/buscar')
		.post(home.AllData)
	app.route('/situacao')
		.post(home.salvarSituacao)
	app.route('/pesquisar')
		.post(home.Pesquisar)	
	app.route('/historico')
		.post(home.Historico)	
	// app.route('/proposta')
	// 	.post(home.buscarPropEnviadas)	
	app.route('/email')
		.post(home.enviarEmail)

}