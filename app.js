var express          = require('express');
var path             = require('path');
var favicon          = require('serve-favicon');
var logger           = require('morgan');
var cookieParser     = require('cookie-parser');
var bodyParser       = require('body-parser');
var session          = require('express-session');
var consign          = require('consign');
var mongoose         = require('mongoose');
var flash            = require('express-flash');
var moment           = require('moment');
var expressValidator = require('express-validator');
var feed             = require("feed-read");
// var nodemailer       = require('nodemailer');


// CONEXÃO COM O BANCO
mongoose.connect('mongodb://localhost/megaRefrigeracao', function(err){
  if(err){
    console.log("Erro ao conectar no mongodb "+err);
  }else{
    console.log("Conexão com mongodb efetuada com sucesso!");
  }
})
// var routes = require('./routes/index');
// var users  = require('./routes/users');

var app = express();
var erros = require('./middleware/erros');

//middleware

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

// app.use(favicon());
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(expressValidator());
app.use(cookieParser());
app.use(session({ secret: '161solucoes' }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());

//helpers
app.use(function(req, res, next){
  res.locals.session = req.session.admin;
  res.locals.sessionAdmin = req.session.loginAdmin;
  res.locals.isLogged = req.session.admin ? true : false;
  res.locals.isLoggedAdmin = req.session.loginAdmin ? true : false;
  res.locals.moment = moment;
  next();
});
// app.use(function(req, res, next){
//   res.locals.session = req.session.loginAdmin;
//   res.locals.isLogged = req.session.loginAdmin ? true : false;
//   res.locals.moment = moment;
//   next();
// });
consign().then('models').then('controllers').then('routes').into(app);

//middleware
// app.use(erros.notfound);
// app.use(erros.serverError);
app.listen(80, function() {
    console.log('Express server listening on port 80');
});
