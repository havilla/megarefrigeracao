var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

module.exports = function(){
	var InfoAdm = mongoose.Schema({
		data_cad   : {type:Date, default: Date.now},		
		situacao   : {type: String},
		descricao  : {type:String},
		nomeAdmin  : {type: String},
		id_Admin   : {type: String}
	});	
	var Fotos = mongoose.Schema({
		fotosPro  : {type: String},
		fotosNome : {type: String}
	});
	var Produtos = mongoose.Schema({
		nomePro      : {type: String},
		descricaoPro : {type: String},
		qtdePro      : {type: String},
		fotos        : [Fotos],
		infoAdm		 : [InfoAdm]
	});
	var orcamentoSchema = mongoose.Schema({
		categoria : {type: String},
		nome      : {type: String},
		email     : {type: String},
		telefone  : {type: String},
		proposta  : {type: String},
		produtos  : [Produtos],
		anexoOrc  : {type: String},
		estadoPed : {type: String},
		data_cad  : {type:Date, default: Date.now}
	});
	return mongoose.model('orcamento', orcamentoSchema);
}