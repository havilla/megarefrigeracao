var mongoose = require('mongoose');
module.exports = function(){
	var ImagemSlider = mongoose.Schema({
		anexo 						: 	{type:String},
		nome						: 	{type:String}
	});
	
	var slidersecundSchema = mongoose.Schema({
		imagens						: [ImagemSlider]
		// nome						: 	{type:String}
	});
	return mongoose.model('Slidersecund', slidersecundSchema);
}
