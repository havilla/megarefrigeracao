var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

module.exports = function(){
	var adminSchema = mongoose.Schema({
		nome    : {type: String},
		email   : {type: String, trim: true, unique: true, index: true},
		password:{type: String},
		// site    : {type: String},
		data_cad:{type:Date, defaut: Date.now}
	});

	adminSchema.methods.generateHash = function(password){
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	};

	adminSchema.methods.validPassword = function(password, old_password){
		return bcrypt.compareSync(password, old_password, null);
	}

	return mongoose.model('Admin', adminSchema);
}