var mongoose = require('mongoose');
module.exports = function(){
	var ImagemImgs = mongoose.Schema({
		anexo 						: 	{type:String},
		nome						: 	{type:String}
	});
	
	var servicoSchema = mongoose.Schema({
		imagens						: [ImagemImgs]
		// nome						: 	{type:String}
	});
	return mongoose.model('servico', servicoSchema);
}
