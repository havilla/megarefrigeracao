var mongoose = require('mongoose');
module.exports = function(){
	var endereco = mongoose.Schema({
		logradouro		 : 	{type:String, trim: true},
		complemento		 : 	{type:String, trim: true},
		bairro		     : 	{type:String, trim: true},
		cep		         : 	{type:String, trim: true},
		numero		     : 	{type:String, trim: true},
	});
	var parceirosSchema = mongoose.Schema({
		nomeEmpresa		 : 	{type:String, trim: true},
		nome	         : 	{type:String, trim: true},
		email		     : 	{type:String, trim: true, unique : true, index :true},
		telefone		 : 	{type:String, trim: true},
		descricao		 : 	{type:String, trim: true},
		endereco         :  [endereco],
		estado		     : 	{type:String, trim: true},
		cidade		     : 	{type:String, trim: true},		
		latitude		 : 	{type:String, trim: true},
		longitude	     : 	{type:String, trim: true},
		foto             :  {type:String},
		data_cad	     : 	{type:Date, default: Date.now}
	});
	return mongoose.model('parceiros', parceirosSchema);
}
