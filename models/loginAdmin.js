var mongoose = require('mongoose');
var bcrypt	 = require('bcrypt-nodejs');

module.exports = function(){
	var loginAdminSchema = mongoose.Schema({
		nome		:	{type:String, trim: true},
		email		:	{type:String, trim: true, unique : true, index :true},
		password	:	{type:String},
		data_cad	:	{type:Date, default: Date.now}
	});
	loginAdminSchema.methods.generateHash = function(password){
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8),null);
	}
	loginAdminSchema.methods.validPassword = function(password, old_password){
		return bcrypt.compareSync(password,old_password, null); 
	}
	return mongoose.model('loginAdmin', loginAdminSchema);
}
