var mongoose = require('mongoose');

module.exports = function(){
	var emailAdminSchema = mongoose.Schema({
		email		:	{type:String, trim: true, unique : true, index :true},
		data_cad	:	{type:Date, default: Date.now}
	});
	return mongoose.model('emailAdmin', emailAdminSchema);
}
