module.exports = function(app) {
	var Fotosempresa = app.models.fotosempresa;
	var HomeController = {
		index: function(req, res) {
			res.render('admin/fotosempresa');
		},
		salvar: function(req, res) {
			var model = new Fotosempresa();
			model.imagens = req.body.imagens
			if(req.body._id){
				Fotosempresa.findById(req.body._id, function(err,data){
					var model = data;
					model.imagens = req.body.imagens;
					model.save(function(err){
						if(err){
							req.flash('erro', 'Erro ao salvar: '+err);
						}else{
							res.send({
							success: true
						});
						}
					});
				});
			}else{
				model.imagens = req.body.imagens;
				model.save(function(err){
					if(err){
						req.flash('erro', 'Erro ao salvar: '+err);
					}else{
						res.send({
							success: true
						});
					}
				});
			}
		},
		buscar: function(req, res){
			Fotosempresa.find(function(err,dados){
				if(err){
					req.flash('erro', 'Erro ao buscar imagens: '+err);
				}else {
					res.send(dados)
				}
			});
		}
	}
	return HomeController;
}