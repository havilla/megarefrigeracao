module.exports = function(app) {
		var validacao = require('../validacoes/usuarios');
		var autenticacao 	= require('../validacoes/autenticacao');
		var Admin = app.models.admin;
		var AdminController = {
				index: function(req, res) {
					Admin.find(function(err,dados){
					if(err){
						req.flash('erro', 'Erro ao buscar usuários:'+err);
						res.redirect('admin/index');
					}else{
						res.render('admin/home', {lista: dados});				
					}
					// res.render('admin/index');
				});
			},
				login: function(req, res){
					res.render('admin/login');
			},
				autenticacao: function(req, res) {
					var admin = new Admin();
					var email = req.body.email;
					var password = req.body.password;

				if (autenticacao(req, res)) {
					Admin.findOne({'email': email}, function(err, data) {
							if (err) {
								req.flash('erro', 'Erro ao entrar no sistema: ' + err);
								res.redirect('/login');
							} else if (!data) {
								req.flash('erro', 'E-mail não encontrado');
								res.redirect('/login');

							} else if (!admin.validPassword(password, data.password)) {
								req.flash('erro', 'Senha não confere!');
								res.redirect('/login');
							} else {
								req.session.admin = data;
								res.redirect('/sliderprinc');

							}
						});

					}else{
						res.redirect('/');

					}
				},
				salvar: function(req, res) {
					var model 		= new Admin();
					model.email		= req.body.email;
					model.password	= model.generateHash(req.body.password);
						model.save(function(err) {
							if (err) {
								req.flash('erro', 'Erro ao cadastrar: ' + err);
								// res.render('admin/create', {user: req.body});
							} else {
								req.flash('info', 'Registro cadastrado com sucesso!');
								// res.redirect('/usuarios');
							}
						});
					},
				logout: function(req,res){
					req.session.destroy();
					res.redirect('/login');
				},
				edit: function(req, res){
					Admin.findById(req.params.id, function(err,data){
						if(err){
							req.flash('erro', 'Erro ao editar' + err);
							res.redirect('/home');
						}else{
							req.flash('info', 'Registro editado com sucesso!');
							res.render('admin/telaAdmin/usuarioEdit',{dados: data});
						}
					})
				},
				update: function(req, res) {
					if (validacao(req, res)) {
						Admin.findById(req.params.id, function(err, data) {
							var model = data;
							model.nome = req.body.nome;
							model.save(function(err) {
								if (err) {
									req.flash('erro', 'Erro ao editar' + err);
									res.render('admin/telaAdmin/usuarioEdit', {dados: model});
								} else {
									req.flash('info', 'Registro editado com sucesso!');
									res.redirect('/home');
								}
							});
						});
					}else{
						res.render('admin/telaAdmin/usuarioEdit',{user: req.body});

					}
				}
		}
		return AdminController;
	}