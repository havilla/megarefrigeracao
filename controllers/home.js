module.exports = function(app) {
	var moment = require('moment');
	var nodemailer  = require('nodemailer');
	var validacao = require('../validacoes/autenticacao');
	var Usuario = app.models.admin;
	var Orcamento = app.models.orcamento;
	var emailAdmin = app.models.emailAdmin;
	var fs = require('fs');
	var dir = 'public/images/sliderPrinc';
	var HomeController = {
		index: function(req, res) {
			var contImg = 0;
			// function(req, res, next) {
			fs.readdir(dir, (err, files) => {
				contImg = files.length;
				res.render('index', {cont: contImg});
			});
			// }
		},
		indexProduto: function(req, res) {
			res.render('orcamento');
		},
		// indexOrcamento: function(req, res) {
		// 	res.render('admin/orcamento');
		// },
		salvarOrcamento: function(req, res){
			var model        = new Orcamento();
			model.categoria  = req.body.categoria;
			model.nome 	     = req.body.nome.toLowerCase();
			model.email 	 = req.body.email.toLowerCase();
			model.telefone 	 = req.body.telefone.replace(/[^\d]+/g,'');
			model.produtos 	 = req.body.produtos;
			model.estadoPed  = 'ativo';
			var aux = true;
			//Validar extensao do arquivo
			var extensoes_permitidas;
			for (var i = 0; i < req.body.produtos.length; i++) {
				for (var j = 0; j < req.body.produtos[i].fotos.length; j++) {
					if(req.body.categoria=="produto"){
						extensoes_permitidas = new Array("jpeg", "png");
					}else{
						extensoes_permitidas = new Array("doc", "pdf", "plain");
					}
					var stringFoto = req.body.produtos[i].fotos[j].fotosPro;
					extensao = (stringFoto.substring(stringFoto.search("/")+1, stringFoto.search(";")));
					// permitida = false;
					for (var k = 0; k < extensoes_permitidas.length; k++) { 
							console.log('extensao')
							console.log(extensao)
							console.log('extensoes_permitidas[k]')
							console.log(extensoes_permitidas[k])
				         if (extensoes_permitidas[k] == extensao) { 
				         // permitida = true;
				        	 aux = true;
				         break; 
				         }else if((extensoes_permitidas.length-1)==k){
				         	aux = false;
				         }

				      } 
				}
			}
			if(aux){
				model.save(function(err) {
					if (err) {
						req.flash('erro', 'Erro ao salvar: ' + err);
						// res.render('/', {infor: req.body});
					} else {
						req.flash('info', 'Orcamento cadastrado com sucesso!');
						res.json({success: true, message: 'Orcamento Salvo'});
					}
				});
			}else {

				res.json({success: false, message: 'Formato Invalido!Os formatos permitidos são: '+extensoes_permitidas});
			}

		},
		salvarSituacao:function(req,res){
			var model = new Orcamento();
			model._id = req.body._id;
			Orcamento.findById(req.body._id, function(err,data){
					var model = data;
					req.body.produtos.forEach( function(element, index) {
						if(model.produtos[index].infoAdm.length && model.produtos[index].infoAdm[model.produtos[index].infoAdm.length-1] == req.body.produtos[index].situacao && model.produtos[index].infoAdm[model.produtos[index].infoAdm.length-1].descricao == req.body.produtos[index].descricao){
						}else{
							// console.log(req.body.produtos[index].situacao)
							if(req.body.produtos[index].situacao == "prospEnv"){
								model.proposta = "Proposta Enviada";
							}
							var info = {};
							info.situacao = req.body.produtos[index].situacao
							info.descricao = req.body.produtos[index].descricao
							info.nomeAdmin  = req.body.produtos[index].nomeAdmin
							info.id_Admin  = req.body.produtos[index].id_Admin
							model.produtos[index].infoAdm.push(info)
						}
					});
					if(req.body.anexoOrc){
						model.anexoOrc = req.body.anexoOrc;
					}
					model.estadoPed = req.body.estadoPed;
					model.save(function(err){
						if(err){
							// req.flash('erro', 'Erro ao editar: '+err);
							// req.render('usuarios/edit', {dados: model});
							res.json({success: false, message: 'Situacao não preenchida'});
						}else{
							req.flash('info', 'Situação cadastrado com sucesso!');
							res.json({success: true, message: 'Situacao Salvo'});
						}
					});
					// res.send({	
					// 	success: true
					// });
				});
		},
		buscarDados:function(req, res){
			res.render('admin/orcamento');
		},
		AllData:function(req, res){
			Orcamento.findOne({'_id': req.body.id}, function(err, data) {
				res.send(data);
			});
		},
		Pesquisar:function(req, res){
			var busca = req.body;
			var pesquisa = {};
			var inicio = 0;
			if (busca.nome){
				pesquisa.nome = {$regex : '.*'+busca.nome+'.*'}
			} 
			if (busca.email)
			{
				pesquisa.email = {$regex : '.*'+busca.email+'.*'}
			}  
				// pesquisa.nome = '.*'+busca.nome+'.*';
			if (busca.telefone) {
				pesquisa.telefone = {$regex : '.*'+busca.telefone.replace(/[^\d]+/g,'')+'.*'}
			}
			if (busca.categoria) {
				pesquisa.categoria = {$regex : '.*'+busca.categoria+'.*'}
			}			
			if (busca.proposta) {
				pesquisa.proposta = {$regex : '.*'+busca.proposta+'.*'}
			}
			if (!(busca.data_cad_inicio == undefined) && busca.data_cad_inicio  != '--') {
				var data_inicio  = new Date(busca.data_cad_inicio+"T00:00:00.000Z");
				var data_fim = new Date(busca.data_cad_fim+"T23:59:59.000Z");
				pesquisa.data_cad = {$gte: data_inicio, $lte: data_fim}
			}
			if(busca.inicio){
				inicio = req.body.inicio;
			}
			pesquisa.estadoPed = {$regex : '.*ativo.*'}
			if(req.body.estadoPed == 'finalizado'){
				pesquisa.estadoPed = {$regex : '.*finalizado.*'}
			}
			Orcamento.count(pesquisa,function(err, count){
				if(count<=10){
					Orcamento.find(pesquisa)
						.select('nome')
						.select('email')
						.select('telefone')
						.select('categoria')
						.select('proposta')
						.select('data_cad')
						.sort({data_cad: -1})
						.limit(10)
						.exec(function(err,dados) {
						if (err) {
							req.flash('erro', 'Erro ao visualizar reconhecida' + err);
							res.redirect('/orcamento');
						} else {
							res.json({Data: dados,Contador: count, success: true, message: 'Orcamento Salvo'});
						}
					})
				}else {
					Orcamento.find(pesquisa)
						.select('nome')
						.select('email')
						.select('telefone')
						.select('categoria')
						.select('proposta')
						.select('data_cad')
						.sort({data_cad: -1})
						.skip(inicio)
						.limit(10)
						.exec(function(err,dados) {
						if (err) {
							req.flash('erro', 'Erro ao visualizar reconhecida' + err);
							res.redirect('/orcamento');
						} else {
							res.json({
								Data: dados,
								Contador: count,
								success: true,
								message: 'Orcamento Salvo'
							});
						}
					})	
				}
			})
		},
		Historico:function(req, res){
			Orcamento.findById(req.body.id, function(err, data){
				// console.log(data.produtos[req.body.posicao].infoAdm)
				res.send(data.produtos[req.body.posicao].infoAdm);	
			});
		},
		enviarEmail:function(req,res){
			emailAdmin.find(function(err,dados){
				var emailTo="";
				dados.forEach( function(element, index) {
					emailTo = emailTo+element.email+',';
				});
				// console.log(dados)
				// if(err){
				// 	req.flash('erro', 'Erro ao buscar usúarios: '+err);
				// 	res.redirect('emailAdmin');
				// }else{
				// 	res.render('admin/telaAdmin/emailAdmin', {lista: dados}); 
				// }
			// O primeiro passo é configurar um transporte para este
			// e-mail, precisamos dizer qual servidor será o encarregado
			// por enviá-lo:
			var transporte = nodemailer.createTransport({
				// pop.terra.com.br porta 110
				// smtp.terra.com.br 
			  service: 'Gmail', // Como mencionei, vamos usar o Gmail
			  auth: {
			    user: 'orcamento.megarefrigeracao@gmail.com', // Basta dizer qual o nosso usuário
			    pass: 'contmeg4'             // e a senha da nossa conta
			  } 
			});
			// var transporte = nodemailer.createTransport({
			// 	host: 'smtp.terra.com.br',
			// 	port: 110,
			// 	secure: false,
			// 	auth: {
			// 		user: 'contato@megarefrigeracao.com.br',
			// 		pass: 'contmeg4'
			// 	}
			// });
			// Após configurar o transporte chegou a hora de criar um e-mail
			// para enviarmos, para isso basta criar um objeto com algumas configurações
			var inicio = req.body.anexo.search(',');
			var anexo = req.body.anexo.slice(inicio+1);
			// console.log(req.body)
			// console.log(req.body.anexo.slice(inicio+1))
			var email = {
			  // from: ''+req.body.email+'', // Quem enviou este e-mail
			  // from: 'hhavilla@gmail.com', // Quem enviou este e-mail
			  // to: ''+req.body.email+'', // Quem 
			  to: ''+emailTo+''+req.body.email+'',
			  subject: 'Orcamento',  // Um assunto bacana 🙂 
			  html: 'Nome do cliente: '+req.body.nome+'',// O conteúdo do e-mail
			  attachments:[
				{   // utf-8 string as an attachment
		            filename: ''+req.body.nomeArq+'',
		            content: ''+anexo+'',
		            encoding: 'base64'
		        }
			  ]
			  // html: '<div style="background:url('+req.body.anexo+')"></div>'// O conteúdo do e-mail
			};

			// Pronto, tudo em mãos, basta informar para o transporte
			// que desejamos enviar este e-mail
			transporte.sendMail(email, function(err, info){
		        if(err){
					res.sendStatus(400);
		        }else{
		        	res.json({
						Data: info,
						success: true,
						message: 'Email Enviado!'
					});
					res.sendStatus(200);
		        }
				});	
			});
		}
	}
	return HomeController;
}
	// diogo@megarefrigeracao.com.br