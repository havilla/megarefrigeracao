module.exports = function(app) {
	var Distribuidores = app.models.distribuidores;
	var HomeController = {
		index: function(req, res) {
			res.render('admin/distribuidores');
		},
		salvar: function(req, res) {
			var model = new Distribuidores();
			model.imagens = req.body.imagens;
			Distribuidores.find(function(err,dados){
				if(dados.length){
					Distribuidores.findById(dados[0]._id, function(err,data){
						var model = data;
						model.imagens = req.body.imagens;
						model.save(function(err){
							if(err){
								req.flash('erro', 'Erro ao salvar: '+err);
							}else{
								res.send({
								success: true
							});
							}
						});
					});
				}else{
					model.imagens = req.body.imagens;
					model.save(function(err){
						if(err){
							req.flash('erro', 'Erro ao salvar: '+err);
						}else{
							res.send({
								success: true
							});
						}
					});
				}
			});
		},
		buscar: function(req, res){
			Distribuidores.find(function(err,dados){
				res.send(dados)
	// 			if(err){
	// 				req.flash('erro', 'Erro ao visualizar usuário: '+err);
	// 				res.render('/usuario');
	// 			}else {
	// 				res.render('usuarios/show', {dados: dados})

				// }
			});
		}
	}
	return HomeController;
}