module.exports = function(app) {
	var Parceiros = app.models.parceiros;
	var validacao 	= require('../validacoes/usuarios');
	var HomeController = {
		parceiros: function(req, res) {
			res.render('admin/telaAdmin/parceiros');
		},
		create: function(req,res){
			res.render('admin/telaAdmin/parceirosCreate');
		},
		post: function(req, res) {
			console.log('entrou post parceiros')
			console.log(req.body)
			if (validacao(req, res)) {
				// console.log("passou na validação")
				var model 		    = new Parceiros();
				model.nomeEmpresa	= req.body.nomeEmpresa;
				model.nome		    = req.body.nome;
				model.email		    = req.body.email;
				model.telefone		= req.body.telefone;
				model.descricao		= req.body.descricao;
				model.endereco=[{
					'logradouro'  : req.body.logradouro,
					'complemento' : req.body.complemento,
					'bairro'      : req.body.bairro,
					'cep'         : req.body.cep,
					'numero'      : req.body.numero
				}];
				model.estado	= req.body.uf;
				model.cidade	= req.body.cidade;
				model.foto 		= req.body.foto;
				//Salvando a latitude e longitude do link do mapa
				var link = req.body.link;
				model.latitude = link.slice(link.search('m2!3d')+5, link.search('!4d'));
				model.longitude = link.slice(link.search('!4d')+3);
				Parceiros.findOne({"email": model.email}, function(err, data) {
					if (data) {
						req.flash('erro', 'Email encontrase cadastrado.');
						res.render('admin/telaAdmin/parceirosCreate', {user: req.body});
					} else {
						model.save(function(err) {
							if (err) {
								req.flash('erro', 'Erro ao cadastrar: ' + err);
								res.render('admin/telaAdmin/parceirosCreate', {user: req.body});
							} else {
								req.flash('info', 'Registro cadastrado com sucesso!');
								res.redirect('/parceiros');
							}
						});
					}

				});
			}else{
				res.render('admin/telaAdmin/parceirosCreate', {user: req.body});
			}
		},
		buscar: function(req, res){
			console.log(req.body)
			Parceiros.find(req.body)
				.sort({data_cad: -1})
				.limit(10)
				.exec(function(err,dados) {
				if (err) {
					req.flash('erro', 'Erro ao buscar parceiros' + err);
					res.redirect('/');
				} else {
					res.json({Data: dados, success: true, message: 'Parceiros encontrados'});
				}
			});
		},
		buscarId: function(req, res){
			var model = new Parceiros();
			model._id = req.params.id;
			Parceiros.findById(req.params.id, function(err,data){
				if (err) {
					req.flash('erro', 'Erro ao buscar parceiros por ID' + err);
					res.redirect('/');
				} else {
					res.json({Data: data, success: true, message: 'Parceiros encontrados'});
				}
			});
		},
		editar:function(req, res){
			// console.log(req)
			Parceiros.findOne({"email": req.params.email}, function(err,data){
				if(data){
					res.render('admin/telaAdmin/parceirosEdit',{user: data});
				}else{
					req.flash('erro', 'Erro: E-mail não consta na base de dados! Por favor, insira um e-mail valido.');
					res.render('admin/telaAdmin/parceirosCreate');

				}
			})
		},
		update: function(req, res) {
			console.log("entrou update");
			if (validacao(req, res)) {
				Parceiros.findOne({"email": req.params.email}, function(err, data) {
					var model = data;
					console.log(model.nome)
					console.log(req.body.nome)
					model.nomeEmpresa	= req.body.nomeEmpresa;
					model.nome		    = req.body.nome;
					model.email		    = req.body.email;
					model.telefone		= req.body.telefone;
					model.descricao		= req.body.descricao;
					model.endereco=[{
						'logradouro'  : req.body.logradouro,
						'complemento' : req.body.complemento,
						'bairro'      : req.body.bairro,
						'cep'         : req.body.cep,
						'numero'      : req.body.numero
					}];
					model.estado	= req.body.uf;
					model.cidade	= req.body.cidade;
					model.foto 		= req.body.foto;
					if(req.body.link != ''){
						var link = req.body.link;
						model.latitude = link.slice(link.search('m2!3d')+5, link.search('!4d'));
						model.longitude = link.slice(link.search('!4d')+3);
					}
					model.save(function(err) {
						if (err) {
							req.flash('erro', 'Erro ao editar' + err);
							res.render('admin/telaAdmin/parceirosCreate', {dados: model});
						} else {
							req.flash('info', 'Registro editado com sucesso!');
							res.redirect('/home');
						}
					});
				});
			}else{
				res.render('admin/telaAdmin/parceirosCreate',{user: req.body});

			}
		}
}
	return HomeController;
}