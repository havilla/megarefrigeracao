module.exports = function(app) {
	var LoginAdmin = app.models.loginAdmin;
	var Admin = app.models.admin;
	var emailAdmin = app.models.emailAdmin;
	var validacao 	= require('../validacoes/usuarios');
	var autenticacao 	= require('../validacoes/autenticacao');
	var HomeController = {
		index: function(req, res) {
			res.render('admin/telaAdmin/loginAdmin');
		},
		home: function(req, res){
			Admin.find(function(err,dados){
				// console.log(dados)
				if(err){
					req.flash('erro', 'Erro ao buscar usúarios: '+err);
					res.redirect('home');
				}else{
					res.render('admin/telaAdmin/home', {lista: dados}); 
				}
			})
		},
		emailAdmin: function(req, res){
			emailAdmin.find(function(err,dados){
				// console.log(dados)
				if(err){
					req.flash('erro', 'Erro ao buscar usúarios: '+err);
					res.redirect('emailAdmin');
				}else{
					res.render('admin/telaAdmin/emailAdmin', {lista: dados}); 
				}
			})
		},
		create: function(req,res){
			res.render('admin/telaAdmin/UsuarioCreate',{user: new LoginAdmin()});
		},
		emailCreate: function(req,res){
			res.render('admin/telaAdmin/emailCreate',{user: new LoginAdmin()});
		},
		post: function(req, res) {
			if (validacao(req, res)) {
				// console.log("passou na validação")
				var model 		= new Admin();
				model.nome		= req.body.nome;
				model.email		= req.body.email;
				model.site 		= req.body.site;
				model.password	= model.generateHash(req.body.password);
				Admin.findOne({"email": model.email}, function(err, data) {
					if (data) {
						req.flash('erro', 'Email encontrase cadastrado.');
						res.render('admin/telaAdmin/UsuarioCreate', {user: req.body});
					} else {
						model.save(function(err) {
							if (err) {
								req.flash('erro', 'Erro ao cadastrar: ' + err);
								res.render('admin/telaAdmin/UsuarioCreate', {user: req.body});
							} else {
								req.flash('info', 'Registro cadastrado com sucesso!');
								res.redirect('/home');
							}
						});
					}

				});
			}else{
				res.render('admin/telaAdmin/UsuarioCreate', {user: req.body});
			}
		},
		postEmail: function(req, res) {
			if ((req, res)) {
				var model 		= new emailAdmin();
				model.email		= req.body.email;
				emailAdmin.findOne({"email": model.email}, function(err, data) {
					if (data) {
						req.flash('erro', 'Email encontrase cadastrado.');
						res.render('admin/telaAdmin/emailCreate', {user: req.body});
					} else {
						model.save(function(err) {
							if (err) {
								req.flash('erro', 'Erro ao cadastrar: ' + err);
								res.render('admin/telaAdmin/emailCreate', {user: req.body});
							} else {
								req.flash('info', 'Registro cadastrado com sucesso!');
								res.redirect('/emailAdmin');
							}
						});
					}

				});
			}else{
				res.render('admin/telaAdmin/emailCreate', {user: req.body});
			}
		},
		autenticacao: function(req, res) {
			var loginAdmin = new LoginAdmin();
			var email = req.body.email;
			var password = req.body.password;

		if (autenticacao(req, res)) {
			LoginAdmin.find(function(err, data){
				// console.log(data)
			});
			LoginAdmin.findOne({'email': email}, function(err, data) {
					if (err) {
						req.flash('erro', 'Erro ao entrar no sistema: ' + err);
						res.redirect('/loginAdmin');
					} else if (!data) {
						// console.log(req.body)
						req.flash('erro', 'E-mail não encontrado');
						res.redirect('/loginAdmin');

					} else if (!loginAdmin.validPassword(password, data.password)) {
						req.flash('erro', 'Senha não confere!');
						res.redirect('/loginAdmin');
					} else {
						req.session.loginAdmin = data;
						res.redirect('/home');
					}
				});

			}else{
				res.redirect('/');

			}
		},
		sair: function(req,res){
			req.session.destroy();
			res.redirect('/loginAdmin');
		},
		show: function(req, res){
			emailAdmin.findById(req.params.id, function(err,dados){
				if(err){
					req.flash('erro', 'Erro ao visualizar usuário: '+err);
					res.render('/admin/telaAdmin/emailAdmin');
				}else {
					res.render('admin/telaAdmin/emailExcluir', {dados: dados})
					
				}
			});
		},
		delete: function(req, res){
			emailAdmin.remove({_id: req.params.id}, function(err){
				if(err){
					req.flash('erro', 'Erro ao excluir usuário: '+err);
					res.render('/admin/telaAdmin/emailAdmin');
				}else{
					req.flash('info', 'Registro excluido com sucesso!');
					res.redirect('/emailAdmin');
				}
			});
		}
	}
	return HomeController;
}