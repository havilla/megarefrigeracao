module.exports = function (req, res, next) {
	if(req.session.admin){
		return next();
	}
	return res.redirect('/login');
}