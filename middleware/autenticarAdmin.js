module.exports = function (req, res, next) {
	if(req.session.loginAdmin){
		return next();
	}
	return res.redirect('/loginAdmin');
}